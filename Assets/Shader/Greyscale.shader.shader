﻿Shader "Custom/Greyscale" {

	Properties {
		_MainTex ("", 2D) = "white" {}
		_Power ("", Float) = 1.0
	}
	
	SubShader {
	
		ZTest Always Cull Off ZWrite Off Fog { Mode Off }
		
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			
			struct v2f {
				float4 pos : POSITION;
				half2 uv : TEXCOORD0;
			};
			
			
			v2f vert (appdata_img v){
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv = MultiplyUV (UNITY_MATRIX_TEXTURE0, v.texcoord.xy);
				return o;
			}
			
			sampler2D _MainTex;
			float _Power;
			
			
			fixed4 frag (v2f i) : COLOR{
				fixed4 orgCol = tex2D(_MainTex, i.uv);
				
				
				float avg = (orgCol.r + orgCol.g + orgCol.b)/3;
				fixed4 col = lerp( orgCol, fixed4(avg, avg, avg, 1), _Power );
				
				return col;
			}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
