
//
// Copyright (C) 2013 Sixense Entertainment Inc.
// All Rights Reserved
//

using UnityEngine;
using System.Collections;

public class SixenseHandController : SixenseObjectController
{
	protected Animator			m_animator = null;
	protected float				m_fLastTriggerVal = 0.0f;
	
	protected override void Start() 
	{
		// get the Animator
		m_animator = this.gameObject.GetComponent<Animator>();
        m_animator.SetBool( "Fist", true );
		base.Start();
	}
	
	protected override void UpdateObject( SixenseInput.Controller controller )
	{
		if ( m_animator == null )
		{
			return;
		}
		
		if ( controller.Enabled )  
		{		
			// Animation update
			UpdateAnimationInput( controller );
		}
				
		base.UpdateObject(controller);
	}
	
	// Updates the animated object from controller input.
	protected void UpdateAnimationInput( SixenseInput.Controller controller)
	{	
		// Fist
		m_animator.SetBool( "Fist", true );
	}
}

