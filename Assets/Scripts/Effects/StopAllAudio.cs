﻿using UnityEngine;
using System.Collections;

public class StopAllAudio : MonoBehaviour {

    private AudioSource[] allAudioSources;

    void Start(){
        allAudioSources = FindObjectsOfType<AudioSource>() as AudioSource[];
    }

    public void StopAll(){
        foreach(var audio in allAudioSources)
            audio.Stop();
    }
}
