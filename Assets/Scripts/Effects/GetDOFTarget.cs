﻿using UnityEngine;
using System.Collections;

public class GetDOFTarget : MonoBehaviour {

    public GameObject focusTarget;

	void Update () {
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity))
            focusTarget.transform.position = hit.point;
	}
}
