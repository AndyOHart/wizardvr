﻿using UnityEngine;
using System.Collections;

//Handles the custom greyscale shader for player health
public class Greyscale : MonoBehaviour {

    public Material mat;
    private float gScale;

    //Sets the greyscale off
	void Start () {
        gScale = 0.0f;
        mat.SetFloat("_Power",gScale);
	}
  
    //Allows modifcation of the greyscale amount
    public void SetScale(float scale){
        gScale = scale;
        mat.SetFloat("_Power", gScale);
    }

    //Assigns the RenderTexture
    void OnRenderImage(RenderTexture source, RenderTexture destination){
        Graphics.Blit(source,destination,mat);
    }
}
