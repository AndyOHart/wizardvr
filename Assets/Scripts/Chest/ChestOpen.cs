using UnityEngine;
using System.Collections;

public class ChestOpen : MonoBehaviour {
   
    private bool isOpen;

    public GameObject lightPowerup;
    public AudioSource ChestOpenAudio;

    void Start(){
        lightPowerup.SetActive(false);
        isOpen = false;
    }

    //Method opens the chest and shows the light object
    public void OpenChest(){
        lightPowerup.SetActive(true);
        ChestOpenAudio.Play();
        animation["Open"].speed = 0.3f;

        if(!animation.isPlaying)
            animation.Play("Open");       

        isOpen = true;
    }

    //Returns whether chest open
    public bool IsChestOpen(){
        return isOpen;
    }
}
