﻿using UnityEngine;
using System.Collections;

public class Torch : MonoBehaviour {

    //public GameObject torchObject;
    public GameObject flame;

	// Use this for initialization
	void Start () {
        DisableFlame();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void EnableFlame(){
        flame.SetActive(true);
        audio.enabled = true;
    }

    public void DisableFlame(){
        flame.SetActive(false);
        audio.enabled = false;
    }
}
