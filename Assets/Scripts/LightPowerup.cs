﻿using UnityEngine;
using System.Collections;

public class LightPowerup : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision collision){
        CharacterController cc = collision.collider.gameObject.GetComponent<CharacterController>();

        if(cc != null){
           Debug.Log("Hit Character");
            Destroy(this.gameObject);
        }
    }
}
