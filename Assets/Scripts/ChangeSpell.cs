﻿using UnityEngine;
using System.Collections;

public class ChangeSpell : MonoBehaviour {

    //Things that can be set in the inspector
    public Light SpellLight;
    public ParticleSystem WandTip;
    public AudioSource ChangeFire;
    public AudioSource ChangeLight;
    public AudioSource ChangePhysics;
    public AudioSource ChangeHeal;

    //The selected button
	private int selectedButton;

    //Colours
    private Color lightSpellColour = new Color(0.953f,0.929f,0.167f);
    private Color fireSpellColour = new Color(1.0f,0.443f,0.047f);
    private Color physicsSpellColour = new Color(0.090f,0.462f,0.729f);
    private Color healingSpellColour = new Color(0.047f,1.0f,0.258f);

    //Constants
    private const int BUTTON_ONE = 1;
    private const int BUTTON_TWO = 2;
    private const int BUTTON_THREE = 3;
    private const int BUTTON_FOUR = 4;

    private bool isBusy;

    void Start(){
        selectedButton = 3;
        isBusy = false;
    }

	// Update is called once per frame
	void Update () {
        if(SixenseInput.Controllers[1].GetButtonUp(SixenseButtons.THREE) && (selectedButton != BUTTON_THREE) && isBusy == false){
            SpellLight.color = lightSpellColour;
            WandTip.startColor = lightSpellColour;
            ChangeLight.Play();
            selectedButton = BUTTON_THREE;
        }
        if(SixenseInput.Controllers[1].GetButtonUp(SixenseButtons.ONE) && (selectedButton != BUTTON_ONE) && isBusy == false){
            SpellLight.color = fireSpellColour;
            WandTip.startColor = fireSpellColour;
            ChangeFire.Play();
            selectedButton = BUTTON_ONE;
        }
        if(SixenseInput.Controllers[1].GetButtonUp(SixenseButtons.TWO) && (selectedButton != BUTTON_TWO) && isBusy == false){
            SpellLight.color = physicsSpellColour;
            WandTip.startColor = physicsSpellColour;
            ChangePhysics.Play();
            selectedButton = BUTTON_TWO;
        }
        if(SixenseInput.Controllers[1].GetButtonUp(SixenseButtons.FOUR) && (selectedButton != BUTTON_FOUR) && isBusy == false){
            SpellLight.color = healingSpellColour;
            WandTip.startColor = healingSpellColour;
            ChangeHeal.Play();
            selectedButton = BUTTON_FOUR;
        }
	}

    public void SetIsBusy(bool ib){
        isBusy = ib;
    }

    public int GetSelectedSpellNumber(){
        return selectedButton;
    }
}
