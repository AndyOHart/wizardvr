﻿using UnityEngine;
using System.Collections;

public class BoulderTrigger : MonoBehaviour {

    public GameObject BoulderObject;

    private void OnTriggerEnter(Collider collider){
        BoulderObject.SetActive(true);
        BoulderObject.rigidbody.AddForce(15f * transform.forward);
    }
}
