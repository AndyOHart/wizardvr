using UnityEngine;
using System.Collections;

//Handles torches

public class Torch : MonoBehaviour {

    //public GameObject torchObject;
    public GameObject flame;
    public AudioSource FireAudio;
    public bool startOn;

	//If startOn enabled, the flame starts on
	void Start () {
        if(startOn)
            EnableFlame();
        else
            DisableFlame();
	}

    //Enables the flame
    public void EnableFlame(){
        flame.SetActive(true);
        FireAudio.Play();
    }

    //Disables the flame
    public void DisableFlame(){
        flame.SetActive(false);
        FireAudio.Stop();
    }
}
