﻿using UnityEngine;
using System.Collections;

//Used for the light powerup

public class LightPowerup : MonoBehaviour {

    public AudioClip powerupSound;
    private Vector3 pointToPlaySound;
    private float TimeToAdd = 5f;

    //Handles colliding with player, which plays the sound and destroys the object
    void OnTriggerEnter(Collider collider){
        CharacterController cc = collider.gameObject.GetComponent<CharacterController>();
        PlayerData playerData = GameObject.Find("Wand").GetComponent<PlayerData>();

        if(cc != null){
            playerData.AddLightPower(TimeToAdd);
            pointToPlaySound =  transform.position;
            Destroy(this.gameObject); 

            if(powerupSound)
                AudioSource.PlayClipAtPoint(powerupSound,pointToPlaySound);
        }
    }
}
