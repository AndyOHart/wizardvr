﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

    public AudioSource InSlowMo;
    private AudioSource[] AllAudioSources;

    //Gets all audiosources
    void Awake(){
        AllAudioSources = FindObjectsOfType<AudioSource>() as AudioSource[];
    }

    //Sets the audio pitch for all audio sources
    public void SetAudioPitch(float pitchToSet){
        foreach(AudioSource audio in AllAudioSources){
            if(audio.name == InSlowMo.name)
                continue;
            audio.pitch = pitchToSet;
        }
    }

    //Stops all audiosources 
    public void StopAll(){
        foreach(var audio in AllAudioSources)
            audio.Stop();
    }
}
