﻿using UnityEngine;
using System.Collections;

//This class handles the wand controlling hand information, using the SixenseHandController class

public class WandController : SixenseHandController {

    private Quaternion wandRotation;
    private GameSetupData gameSetupData;
    private GameObject wizardController;

    //Sets up rotation and finds the controller 
    void Start(){
        base.Start();
        wandRotation = transform.rotation;
        wizardController = GameObject.Find("Character_OculusAndHydra");
        FindSetupData();
    }

    //Updates the hydras information
    protected override void UpdateObject(SixenseInput.Controller hydra){

        //These statements must be here as we are overriding the hydra data
        if(!FindSetupData())
            return;

        if(m_animator == null)
            return;

        if(hydra.Enabled)
            UpdateAnimationInput(hydra);

        if(!m_enabled && gameSetupData.State == GameSetupData.CameraState.Enabled)
            m_enabled = !m_enabled;

        //If the controller is enabled, updated position and rotation depending on the wizard controller and offset data
        if(m_enabled){
            gameObject.transform.position = wizardController.transform.position - gameSetupData.currentLeftOffset + gameSetupData.wandPosition;
            gameObject.transform.rotation = gameSetupData.wandRotation * wandRotation;
        }
    }

    void OnGUI(){}

    public Vector3 GetInterationPosition(){
        return transform.position + transform.forward * 0.1f;
    }

    public bool IsHandEnabled(){
        return m_enabled;
    }

    private bool FindSetupData(){
        if(gameSetupData == null){
            GameObject setupObject = GameObject.Find("GameSetupData");
            if(setupObject == null)
                return false;
            gameSetupData = setupObject.GetComponent<GameSetupData>();
        }
        return true;
    }
}
