﻿using UnityEngine;
using System.Collections;

//Represents data related to the player

public class PlayerData:MonoBehaviour {

    public Health PlayerHealth;
    public const float MAX_LIGHT_POWER = 10f;
    public float LightPower = 0f;

    private bool HasPower;

    void Awake(){
        PlayerHealth = GameObject.Find("Character_OculusAndHydra").GetComponent<Health>();
        LightPower = MAX_LIGHT_POWER;
        HasPower = true;
    }

    public void AddLightPower(float timeToAdd){
        LightPower += timeToAdd;
    }

    public void SetHasPower(bool hasPower){
        HasPower = hasPower;
    }

    public bool GetHasPower(){
        return HasPower;
    }
}
