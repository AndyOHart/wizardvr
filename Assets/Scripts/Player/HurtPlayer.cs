﻿using UnityEngine;
using System.Collections;

public class HurtPlayer : MonoBehaviour {

    //These public variables are used to set an amount to damage and
    //a time to deal damage over
    public float amount;
    public float timeBetweenDamage;
    private float timer = 0;

    //Applies damage to player using defined amount
    public void DealDamage(Health playerHealth){
        if(playerHealth != null){
            playerHealth.DecreaseHealth(amount);
        }
    }

    //Used for accessing the method publicly
    public void DealDamage(Health playerHealth, float healthToTake){
        if(playerHealth != null){
            playerHealth.DecreaseHealth(healthToTake);
        }
    }

    //Applies damage to the player over period of defined time
    private void OnTriggerStay(Collider collider){
        Health playerHealth = collider.gameObject.GetComponent<Health>();   

        if(timeBetweenDamage < timer && playerHealth != null){
            DealDamage(playerHealth);
            timer = 0;
        }
        timer += Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision){
        Health playerHealth = collision.gameObject.GetComponent<Health>(); 

        if(timeBetweenDamage == 0 && playerHealth != null){
            DealDamage(playerHealth);
        }

        if(timeBetweenDamage < timer && playerHealth != null){
            Debug.Log("Hit player");
            DealDamage(playerHealth);
            timer = 0;
        }
    }
}
