﻿using UnityEngine;
using System.Collections;

//This class is used for calibration and updating of the player
//for positional tracking usng the Hydras

public class WizardCamera : MonoBehaviour {

    private Vector3 hydraToWorldScale = new Vector3(0.001f,0.001f,0.001f);
    private float worldFloor = 0.0f;
    private float joystickRotationRate = 90.0f;
    private Vector3 wandOffset = new Vector3(0.0f,0.075f,0.0f);

    private Quaternion startCameraOffset;
    private Vector3 bodyRotationOffset = new Vector3(0,0,0);
    private OVRCameraController cameraController;
    private GameSetupData gameSetupData;

    private bool hasCalibrated;
    private GameObject GameData;

    //Sets up the start mode of the camera, pre calibration, and then
    void Start(){

        startCameraOffset = transform.rotation;
        startCameraOffset.x = 0;
        startCameraOffset.z = 0;
        startCameraOffset = Quaternion.Lerp(startCameraOffset, startCameraOffset,1);
        cameraController = GetComponent<OVRCameraController>();
        cameraController.SetOrientationOffset(startCameraOffset);

        GameData = GameObject.Find("GameSetupData");
        if(GameData == null){
            GameData = new GameObject("GameSetupData");
            gameSetupData = GameData.AddComponent("GameSetupData") as GameSetupData;
            DontDestroyOnLoad(gameSetupData);
        }
        else
            gameSetupData = GameData.GetComponent<GameSetupData>();

        gameSetupData.hydraToFloor = -860;
    }

    //This handles the GUI that is seen on screen for calibrating the Rift and Hydras for positional tracking
    //It runs through each state. After this, you can play the game
    void OnGUI(){

        switch(gameSetupData.State){
            case GameSetupData.CameraState.Disabled:
                GUI.Box(new Rect(Screen.width/2 - 200, Screen.height/2,400,30),"Attach Rift & Hydra to play");
                break;
            case GameSetupData.CameraState.CalibrateFloorDistance:
                GUI.Box(new Rect((7*Screen.width)/32, Screen.height/2,200,60),"Touch right Hydra to floor\nPull the right trigger");
                GUI.Box(new Rect((20*Screen.width)/32, Screen.height/2,200,60),"Touch right Hydra to floor\nPull the right trigger");
                break;
            case GameSetupData.CameraState.CalibrateChestDistance:
                GUI.Box(new Rect((7*Screen.width)/32, Screen.height/2,200,90),"Attach left Hydra to body\nFace forwards\nPull right trigger");
                GUI.Box(new Rect((20*Screen.width)/32, Screen.height/2,200,90),"Attach left Hydra to body\nFace forwards\nPull right trigger");
                break;
            case GameSetupData.CameraState.CalibrateNeckDistance:
                GUI.Box(new Rect((7*Screen.width)/32, Screen.height/2,200,60),"Touch Hydra to back of the neck\nPull right trigger");
                GUI.Box(new Rect((20*Screen.width)/32, Screen.height/2,200,60),"Touch Hydra to back of the neck\nPull right trigger");
                break;
            case GameSetupData.CameraState.Enabled:
                hasCalibrated = true;
                break;
        }
    }

    void Update(){

        //If the hydras are placed on base station, recalibrate
        if(SixenseInput.Controllers[0].Docked && SixenseInput.Controllers[1].Docked && hasCalibrated){
            Destroy(GameData);
            hasCalibrated = false;
            Start();
        }

        SixenseInput.Controller leftHydra = SixenseInput.GetController(SixenseHands.LEFT);
        SixenseInput.Controller rightHydra = SixenseInput.GetController(SixenseHands.RIGHT);

        if(leftHydra == null || rightHydra == null)
            return;

        if(SixenseInput.Controllers[0].GetButtonUp(SixenseButtons.START))
            OVRDevice.ResetOrientation(0);

        //Run through each state for calibration
        switch(gameSetupData.State){

            //If its disabled, enable it
            case GameSetupData.CameraState.Disabled:
                if(SixenseInput.IsBaseConnected(0) && OVRDevice.SensorCount > 0){
                    gameSetupData.enabled = true;
                    gameSetupData.State = GameSetupData.CameraState.CalibrateFloorDistance;
                }
                break;
            //Get the distance from the right hydra to the floor
            case GameSetupData.CameraState.CalibrateFloorDistance:
                if(rightHydra.GetButtonUp(SixenseButtons.TRIGGER)){
                    gameSetupData.hydraToFloor = rightHydra.Position.y;
                    gameSetupData.State = GameSetupData.CameraState.CalibrateChestDistance;
                }
                break;
            //Calibrate distance from the base to the left hydra
            case GameSetupData.CameraState.CalibrateChestDistance:
                if(rightHydra.GetButtonUp(SixenseButtons.TRIGGER)){
                    gameSetupData.leftRotationOffset = Quaternion.Inverse(leftHydra.Rotation);

                    Quaternion baseCameraYaw = new Quaternion(0,0,0,0);
                    OVRDevice.GetOrientation(0, ref baseCameraYaw);
                    baseCameraYaw.x = 0;
                    baseCameraYaw.z = 0;
                    baseCameraYaw = Quaternion.Lerp(baseCameraYaw,baseCameraYaw,1);
                    startCameraOffset = startCameraOffset * baseCameraYaw;
                    gameSetupData.State = GameSetupData.CameraState.CalibrateNeckDistance;
                }
                break;
            //Calibrates distance from right hydra to the left hydra
            case GameSetupData.CameraState.CalibrateNeckDistance:
                if(rightHydra.GetButtonUp(SixenseButtons.TRIGGER)){
                    gameSetupData.chestToNeckOffset = rightHydra.Position - leftHydra.Position;
                    gameSetupData.State = GameSetupData.CameraState.Enabled;
                }
                break;
            //Once enabled, Update the buttons and camera each frame
            case GameSetupData.CameraState.Enabled:
                UpdateHydraButtons(leftHydra, rightHydra);
                UpdateHydraCamera(leftHydra, rightHydra);
                startCameraOffset *= Quaternion.AngleAxis(Time.deltaTime * rightHydra.JoystickX * joystickRotationRate, Vector3.up);
                gameSetupData.joystickX = 0;

                Vector3 oldBodyPosition = gameSetupData.bodyPosition;
                UpdateHydraCamera(leftHydra, rightHydra);
                bodyRotationOffset += oldBodyPosition - gameSetupData.bodyPosition;
                cameraController.SetOrientationOffset(startCameraOffset);
                break;
        }
    }

    public bool HasCalibrated(){
        return hasCalibrated;
    }

    //Updates the camera for positional tracking and hand movement
    private void UpdateHydraCamera(SixenseInput.Controller leftHydra, SixenseInput.Controller rightHydra){

        Quaternion fullBodyRotation = startCameraOffset * leftHydra.Rotation * gameSetupData.leftRotationOffset;

        //Normalize to face forward
        Quaternion rotationToFaceForward = fullBodyRotation;
        rotationToFaceForward.x = 0;
        rotationToFaceForward.z = 0;
        rotationToFaceForward = Quaternion.Lerp(rotationToFaceForward, rotationToFaceForward,1);

        //Uses left hydras position to update the body position in game
        Vector3 hydraLocation = new Vector3(leftHydra.Position.x, leftHydra.Position.y - gameSetupData.hydraToFloor, leftHydra.Position.z);
        hydraLocation = startCameraOffset * hydraLocation;
        Vector3 chestToNeckLocal = fullBodyRotation * gameSetupData.chestToNeckOffset;
        hydraLocation += chestToNeckLocal;

        //Scales the hydras location to match the world
        hydraLocation.Scale(hydraToWorldScale);
        gameSetupData.bodyPosition = hydraLocation;
        gameSetupData.bodyPosition.y += worldFloor;

        gameSetupData.bodyRotation = rotationToFaceForward;

        //Moves the wand each frame
        Vector3 wandPosition = new Vector3(rightHydra.Position.x, rightHydra.Position.y - gameSetupData.hydraToFloor,rightHydra.Position.z);

        wandPosition = startCameraOffset * wandPosition;
        wandPosition.Scale(hydraToWorldScale);

        gameSetupData.wandRotation = startCameraOffset * rightHydra.Rotation ;

        //Updates wand and body positions
        gameSetupData.wandPosition = wandPosition + gameSetupData.wandRotation * wandOffset;
        gameSetupData.wandPosition.y += worldFloor;
        gameSetupData.bodyPosition += bodyRotationOffset;
        gameSetupData.wandPosition += bodyRotationOffset;
    }

    //Sets buttons
    private void UpdateHydraButtons(SixenseInput.Controller leftHydra, SixenseInput.Controller rightHydra){

        gameSetupData.triggerPressed = rightHydra.GetButtonDown(SixenseButtons.TRIGGER);
        gameSetupData.triggerReleased = rightHydra.GetButtonUp(SixenseButtons.TRIGGER);
        gameSetupData.trigger = rightHydra.GetButton(SixenseButtons.TRIGGER);

        gameSetupData.bumperPressed = rightHydra.GetButtonDown(SixenseButtons.BUMPER);
        gameSetupData.bumperReleased = rightHydra.GetButtonUp(SixenseButtons.BUMPER);
        gameSetupData.bumper = rightHydra.GetButton(SixenseButtons.BUMPER);

        gameSetupData.button1Pressed = rightHydra.GetButtonDown(SixenseButtons.ONE);
        gameSetupData.button1Released = rightHydra.GetButtonUp(SixenseButtons.ONE);
        gameSetupData.button1 = rightHydra.GetButton(SixenseButtons.ONE);

        gameSetupData.button2Pressed = rightHydra.GetButtonDown(SixenseButtons.TWO);
        gameSetupData.button2Released = rightHydra.GetButtonUp(SixenseButtons.TWO);
        gameSetupData.button2 = rightHydra.GetButton(SixenseButtons.TWO);

        gameSetupData.button3Pressed = rightHydra.GetButtonDown(SixenseButtons.THREE);
        gameSetupData.button3Released = rightHydra.GetButtonUp(SixenseButtons.THREE);
        gameSetupData.button3 = rightHydra.GetButton(SixenseButtons.THREE);

        gameSetupData.button4Pressed = rightHydra.GetButtonDown(SixenseButtons.FOUR);
        gameSetupData.button4Released = rightHydra.GetButtonUp(SixenseButtons.FOUR);
        gameSetupData.button4 = rightHydra.GetButton(SixenseButtons.FOUR);

        gameSetupData.startPressed = rightHydra.GetButtonDown(SixenseButtons.START);
        gameSetupData.startReleased = rightHydra.GetButtonUp(SixenseButtons.START);
        gameSetupData.start = rightHydra.GetButton(SixenseButtons.START);

        gameSetupData.joystickbuttonPressed = rightHydra.GetButtonDown(SixenseButtons.JOYSTICK);
        gameSetupData.joystickbuttonReleased = rightHydra.GetButtonUp(SixenseButtons.JOYSTICK);
        gameSetupData.joystickbutton = rightHydra.GetButton(SixenseButtons.JOYSTICK);

        gameSetupData.joystickX = gameSetupData.bumper ? 0.0f : rightHydra.JoystickX;
        gameSetupData.joystickY = rightHydra.JoystickY;
    }
}