﻿using UnityEngine;
using System.Collections;

//Use this class for sending raycasts to detect what player looks at

public class PlayerLookingAt : MonoBehaviour {

    private Transform cameraTransformation = null;
    private float length = 10.0f;
    private float powerupLength = 1.0f;
    private float chestLength = 2.0f;

    //Get main camera
	void Start () {
        cameraTransformation = Camera.main.transform;
        //cameraTransformation = GameObject.FindWithTag("MainCamera").transform;
	}
	
	//Each frame send out raycasts to check what the player is looking at
	void Update () {

        RaycastHit hit;
        Vector3 rayDirection = cameraTransformation.TransformDirection(Vector3.forward);
        Vector3 rayStart = cameraTransformation.position + rayDirection;

        //If player looks at a weeping angel statue
        if(Physics.Raycast(rayStart, rayDirection, out hit, length)){
            if(hit.collider.name == "Statue"){
                hit.collider.gameObject.GetComponent<WeepingAngel>().enabled = true;
            }
        }

        //If player looks at a light powerup
        if(Physics.Raycast(rayStart, rayDirection, out hit, powerupLength)){
            if(hit.collider.name == "LightPowerup"){
                StartCoroutine(MoveLightPowerup(hit));
            }
        }

        //If player looks at a chest
        if(Physics.Raycast(rayStart, rayDirection, out hit, chestLength)){
            if(hit.collider.name == "Chest"){
                ChestOpen chest = hit.collider.gameObject.GetComponent<ChestOpen>();
                
                if(chest.IsChestOpen() == false)
                    chest.OpenChest();
            }
        }
    }

    //Moves the light powerup towards the player
    IEnumerator MoveLightPowerup(RaycastHit hit){
        float startTime = Time.time;

        while(Time.time - startTime <= 1){
            if(hit.collider != null){
                hit.transform.position = Vector3.Lerp(hit.transform.position, transform.position, Time.time - startTime);
                yield return 1;
            }
            else
                break;  
        }
    }
}
