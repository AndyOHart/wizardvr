﻿using UnityEngine;
using System.Collections;

//This class handles the players health 

public class Health : MonoBehaviour {

    //Health related variables
    private float maxHealth = 100;
    private float minHealth = 0;
    private float currentHealth;
    private bool canHurt = false;
    private float deadTimer = 4.0f;


    //Effect related things
    private float dofAperature;
    private float gScale;
    private Component dofScriptLeft;
    private Component dofScriptRight;
    private Component greyscaleLeft;
    private Component greyscaleRight;
    private MotionBlur motionBlurEffect;
	private MotionBlur motionBlurEffectLeft;
	private MotionBlur motionBlurEffectRight;
    private AudioSource[] allAudioSources;
    //private Component dofScript;
    //private Component greyscale;

    //Public variables to be used with the script
	public AudioSource deathAudio;
	public AudioSource hitAudio;
    public AudioSource heartbeat;
    public Camera oculusLeft;
    public Camera oculusRight;
    public float damageRepeatTime = 3f;
    //public Camera mainCamera;


    //Start method sets current health and gets effect scripts
	void Start () {
        currentHealth = maxHealth;
        dofScriptLeft = oculusLeft.GetComponent("DepthOfFieldScatter");
        dofScriptRight = oculusRight.GetComponent("DepthOfFieldScatter");
        greyscaleLeft = oculusLeft.GetComponent("Greyscale");
        greyscaleRight = oculusRight.GetComponent("Greyscale");
		motionBlurEffectLeft = oculusLeft.GetComponent<MotionBlur>();
		motionBlurEffectRight = oculusRight.GetComponent<MotionBlur>();

        //dofScript = mainCamera.GetComponent("DepthOfFieldScatter");
        //greyscale = mainCamera.GetComponent("Greyscale");
        //motionBlurEffect = mainCamera.GetComponent<MotionBlur>();
	}
	
    //Check if player is dead
	void Update () {;
        if(IsDead()){
			PerformDeathActions();
        }
	}

    //Decreases player health
    public void DecreaseHealth(float amount){
        currentHealth -= amount;
        dofAperature = dofAperature + (amount * 20);
        gScale = gScale + (amount / 100);
        dofScriptLeft.SendMessage("SetAperature", dofAperature);
        dofScriptRight.SendMessage("SetAperature", dofAperature);
        greyscaleLeft.SendMessage("SetScale", gScale);
        greyscaleRight.SendMessage("SetScale", gScale);
        //dofScript.SendMessage("SetAperature", dofAperature);
        //greyscale.SendMessage("SetScale", gScale);

        if(currentHealth <= 50){
            heartbeat.pitch = heartbeat.pitch + (amount/50);
            if(!heartbeat.isPlaying)
                heartbeat.Play();
        }

        if(currentHealth <= 30){
            //motionBlurEffect.enabled = true;
			motionBlurEffectLeft.enabled = true;
			motionBlurEffectRight.enabled = true;
		}

        if(currentHealth > 0)
		    hitAudio.Play();
    }

    //Increases player health
    public void IncreaseHealth(float amount){

        gScale = gScale - (amount / 100);
        dofAperature = dofAperature - (amount * 20);
        dofScriptLeft.SendMessage("SetAperature", dofAperature);
        dofScriptRight.SendMessage("SetAperature", dofAperature);
        greyscaleLeft.SendMessage("SetScale", gScale);
        greyscaleRight.SendMessage("SetScale", gScale);
        //greyscale.SendMessage("SetScale", gScale);

        if(currentHealth <= 50){
            heartbeat.pitch = heartbeat.pitch - (amount/50);
        }
        else
            heartbeat.Stop();

        if(currentHealth > 30){
            //motionBlurEffect.enabled = false;
			motionBlurEffectLeft.enabled = false;
			motionBlurEffectRight.enabled = false;
		}

        if((currentHealth + amount) > maxHealth)
            currentHealth = maxHealth;
        else
            currentHealth += amount;
    }

    //Returns current player health
    public float GetHealth(){
        return currentHealth;
    }

    //Checks if the player has died
    private bool IsDead(){
        if(currentHealth <= minHealth){
			return true;
		}
        
			return false;
    }

    //Turns audio sources on/off
    private void SetAudioSourcesOnOrOff(bool setTo){
        foreach(AudioSource audio in allAudioSources){
            if(audio.isPlaying)
                audio.enabled = setTo;
        }
    }

    //Does stuff when the player has died
	private void PerformDeathActions(){

        heartbeat.Stop();
        allAudioSources = FindObjectsOfType<AudioSource>() as AudioSource[];
        SetAudioSourcesOnOrOff(false);
		
        deathAudio.enabled = true;
        deathAudio.Play();
        CharacterController cc = gameObject.GetComponent<CharacterController>();
        cc.collider.enabled = false;
        //Time.timeScale = 0;
		currentHealth = 100;
	}
}
