﻿using UnityEngine;
using System.Collections.Generic;

//This class is used for controlling the movement in game
[RequireComponent (typeof(CharacterController))]
public class WizardController : OVRComponent {

    protected CharacterController controller = null;
    protected OVRCameraController cameraController = null;
    protected GameSetupData gameSetupData = null;

    private float acceleration = 0.12f;
    private float damping = 0.15f;
    private float jumpForce = 0.3f;
    private float gravityModifier = 0.300f;

    private float moveScale = 1.0f;
    private Vector3 moveThrottle = Vector3.zero;
    private float fallSpeed = -0.20f;

    //When started, find the camera and controller
    new public virtual void Awake(){
        base.Awake();
        FindGameSetupData();
        controller = gameObject.GetComponent<CharacterController>();
        cameraController = gameObject.GetComponentInChildren<OVRCameraController>();
    }

    //Update movement every frame
    new public virtual void Update(){

        base.Update();

        if(!FindGameSetupData())
            return;

        //Changes the location 
        var startLocation = controller.transform.position;
        var leftOffset = gameSetupData.bodyPosition;
        leftOffset.y = 0;
        var offsetApply = leftOffset - gameSetupData.currentLeftOffset;

        controller.Move(offsetApply);
        gameSetupData.currentLeftOffset = leftOffset + (controller.transform.position - (startLocation + offsetApply));
        gameSetupData.currentLeftOffset.y = 0;
        cameraController.transform.position = controller.transform.position + (gameSetupData.bodyPosition - gameSetupData.currentLeftOffset);

        //Sets up the movement acceleration in combination with the joystick push amount
        float moveInfluence = acceleration * 0.1f * DeltaTime;
        moveThrottle.x += gameSetupData.joystickX * moveInfluence;
        moveThrottle.z += gameSetupData.joystickY * moveInfluence;

        //Handles jumping
        if(controller.isGrounded && gameSetupData.joystickbuttonPressed)
            moveThrottle.y += jumpForce;

        //Adds in dampening so you take off and slow down over a small amount of time rather than
        //straight away
        float motorDamp = (1.0f + (damping * DeltaTime));
        moveThrottle.x /= motorDamp;
        moveThrottle.y = (moveThrottle.y > 0.0f) ? (moveThrottle.y / motorDamp) : moveThrottle.y;
        moveThrottle.z /= motorDamp;

        //Sets the direction to move based on body rotation
        Vector3 moveDirection = gameSetupData.bodyRotation * (moveThrottle * DeltaTime);

        //Handles gravity
        if(controller.isGrounded && fallSpeed <= 0)
            fallSpeed = ((Physics.gravity.y * (gravityModifier * 0.002f)));
        else
            fallSpeed += ((Physics.gravity.y * (gravityModifier * 0.002f)) * DeltaTime);

        moveDirection.y += fallSpeed * DeltaTime;

        controller.Move(moveDirection);
    }

    //Sets the flow motion speeds
    public void SetSlowMotionSpeeds(float speed){
        
        acceleration = speed;
    }

    //Finds the setup information
    private bool FindGameSetupData(){

        if(gameSetupData == null){
            GameObject gameSetupObject = GameObject.Find("GameSetupData");
            if(gameSetupObject == null)
                return false;
            gameSetupData = gameSetupObject.GetComponent<GameSetupData>();
        }
        return true;
    }
}






























