﻿using UnityEngine;
using System.Collections;

//This clas is used to setup the Hydras from the start and used in calibration
//It gets set default values here to be changed later

public class GameSetupData : MonoBehaviour {

    public bool enabled = false;
    public bool trigger = false;
    public bool bumper = false;
    public bool button1 = false;
    public bool button2 = false;
    public bool button3 = false;
    public bool button4 = false;
    public bool start = false;
    public bool joystickbutton = false;

    public bool triggerPressed = false;
    public bool bumperPressed = false;
    public bool button1Pressed = false;
    public bool button2Pressed = false;
    public bool button3Pressed = false;
    public bool button4Pressed = false;
    public bool startPressed = false;
    public bool joystickbuttonPressed = false;

    public bool triggerReleased = false;
    public bool bumperReleased = false;
    public bool button1Released = false;
    public bool button2Released = false;
    public bool button3Released = false;
    public bool button4Released = false;
    public bool startReleased = false;
    public bool joystickbuttonReleased = false;

    public Quaternion bodyRotation = new Quaternion(0,0,1,0);
    public Vector3 bodyPosition = new Vector3(0,0,0);
    public Vector3 wandPosition = new Vector3(0,0,0);
    public Quaternion wandRotation = new Quaternion(0,0,1,0);
    public Vector3 currentLeftOffset = new Vector3(0,0,0);

    public float joystickX = 0;
    public float joystickY = 0;

    public RenderTexture helpTextTexture = null;
    public GameObject helpTextObject = null;

    public Quaternion leftRotationOffset;
    public Vector3 chestToNeckOffset;
    public float hydraToFloor;
    public enum CameraState{
        Disabled,
        CalibrateFloorDistance,
        CalibrateChestDistance,
        CalibrateNeckDistance,
        Enabled
    };

    public CameraState State = CameraState.Disabled;
}
