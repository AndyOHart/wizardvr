﻿using UnityEngine;
using System.Collections;

public class LifeSaver : Spell {

    public AudioSource SlowDownAudio;
    public AudioSource SpeedUpAudio;
    public AudioSource Heal;
    public AudioSource FullHealth;
    public Camera OculusLeft;
    public Camera OculusRight;

    private WizardController Movement;
    private AudioManager AudioManager;

    private float CurrentSlowMo = 0;
    private float SlowTimeAllowed = 2.0f;

    //Effects for health
    private GlowEffect GlowEffect;
    private GlowEffect GlowEffectLeft;
    private GlowEffect GlowEffectRight;
    private MotionBlur MotionBlurEffect;
    private MotionBlur MotionBlurLeft;
    private MotionBlur MotionBlurRight;

    private bool SlowMotionEnabled;
    private float HealthToAdd = 5;
    private float TimeBetweenHealthAdditions = 0.5f;
    private float Timer = 0;

    //Setup variables
	void Awake () {
        AudioManager = gameObject.GetComponent<AudioManager>();
        Movement = GameObject.Find("Character_OculusAndHydra").transform.GetComponent<WizardController>();
       
        GlowEffectLeft = OculusLeft.GetComponent<GlowEffect>();
        GlowEffectRight = OculusRight.GetComponent<GlowEffect>();
        MotionBlurLeft = OculusLeft.GetComponent<MotionBlur>();
        MotionBlurRight = OculusRight.GetComponent<MotionBlur>();

        TriggerIsPressed = false;
        SlowMotionEnabled = false;
	}

	protected override void OnUpdate() {

        //When trigger is being pressed, heal the player if they have taken damage
        if (SixenseInput.Controllers [1].Trigger > 0.0){
            ChangeSpell.SetIsBusy(true);

            if(TimeBetweenHealthAdditions < Timer && PlayerData.PlayerHealth.GetHealth() != 100){
                HealEmitter.enableEmission = true;
                HealEmitter.Play();
                PlayerData.PlayerHealth.IncreaseHealth(HealthToAdd);
                Timer = 0;

                if(!Heal.isPlaying)
                    Heal.Play();

                if(PlayerData.PlayerHealth.GetHealth() == 100 && !FullHealth.isPlaying){
                    Heal.Stop();
                    HealEmitter.enableEmission = false;
                    FullHealth.Play();
                }
            }

            Timer += Time.deltaTime;
        }
        else{
            ChangeSpell.SetIsBusy(false);
            HealEmitter.enableEmission = false;
            Heal.Stop();
        }

        //When the bumper is held down, enable slow motion
        if (SixenseInput.Controllers[1].GetButtonDown (SixenseButtons.BUMPER) && TriggerIsPressed == false) {
            SlowMotionEnabled = true;
            Shoot();
            AudioManager.InSlowMo.Play();
            SlowDownAudio.Play();
            AudioManager.InSlowMo.mute = false;
            ChangeSpell.SetIsBusy(true);
        }

        //When released, stop slow motion
        if(SlowMotionEnabled && SixenseInput.Controllers[1].GetButtonUp (SixenseButtons.BUMPER)){
            ChangeSpell.SetIsBusy(false);
            SlowMotionEnabled = false;
            StopSlowMotion();
        }
	}

    //On slow motion start, begin the blur and slow down speeds and sounds
    protected override void Shoot(){
        Time.fixedDeltaTime = 0.050f * 0.02f;
        SetGlowAndBlurState(true);
        Movement.SetSlowMotionSpeeds(0.2f);

        if(Time.timeScale == 1.0f){
            Time.timeScale = 0.3f;
            AudioManager.SetAudioPitch(Time.timeScale);
        }
        if(Time.timeScale == 0.3f)
            CurrentSlowMo += Time.deltaTime;

        if(CurrentSlowMo > SlowTimeAllowed)
            CurrentSlowMo = 0;
    }


    //Sets the glow and blur states to on or off
    private void SetGlowAndBlurState(bool state){
        //glowEffect.enabled = state;
        GlowEffectLeft.enabled = state;
        GlowEffectRight.enabled = state;
        //motionBlurEffect.enabled = state;
        MotionBlurLeft.enabled = state;
        MotionBlurRight.enabled = state;
    }

    //Stops slow motion by resetting the audio and speeds
    private void StopSlowMotion(){
        SetGlowAndBlurState(false);
        Time.timeScale = 1.0f;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
        Movement.SetSlowMotionSpeeds(0.09f);
        AudioManager.InSlowMo.Stop();
        AudioManager.SetAudioPitch(1.0f);
        SpeedUpAudio.Play();
    }
}
