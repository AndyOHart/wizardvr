﻿using UnityEngine;
using System.Collections;

public class SlowMotionSpell : MonoBehaviour {

    public Light currentSpellLight;
    public GameObject spellEmitter;
    public ChangeSpell changeSpell;
    public MovementController movement;
    public AudioSource slowDownAudio;
    public AudioSource speedUpAudio;
    public AudioSource inSlowMo;
    public Camera mainCamera;

    //Spell selection stuff
    private int selectedSpellNum;
    private bool isSelectedSpell;
    private bool triggerIsPressed;

    private float currentSlowMo = 0;
    private float slowTimeAllowed = 2.0f;

    private const int SPELL_NUMBER = 4;

    private GlowEffect glowEffect;
    private MotionBlur motionBlurEffect;

    private bool slowMotionEnabled;

	void Start () {
        glowEffect = mainCamera.GetComponent<GlowEffect>();
        motionBlurEffect = mainCamera.GetComponent<MotionBlur>();
        isSelectedSpell = false;
        triggerIsPressed = false;
        slowMotionEnabled = false;
	}
	
	void Update () {
        CheckIfCurrentSpell();

        //What happens when bumper is pressed
        if (isSelectedSpell && SixenseInput.Controllers[1].GetButtonDown (SixenseButtons.BUMPER) && triggerIsPressed == false) {
            slowMotionEnabled = true;
            StartSlowMotion();
            slowDownAudio.Play();
            inSlowMo.Play();
            inSlowMo.mute = false;
        }

        if(slowMotionEnabled && SixenseInput.Controllers[1].GetButtonUp (SixenseButtons.BUMPER)){
            slowMotionEnabled = false;
            StopSlowMotion();
        }
	}

    private void StartSlowMotion(){
        //Time.timeScale = 0.03f;
        Time.fixedDeltaTime = 0.050f * 0.02f;
        EnableGlowAndBlur();

        movement.SetSlowMotionSpeeds(8,240);

        if(Time.timeScale == 1.0f)
            Time.timeScale = 0.3f;

        if(Time.timeScale == 0.3f)
            currentSlowMo += Time.deltaTime;

        if(currentSlowMo > slowTimeAllowed){
            currentSlowMo = 0;
            Time.timeScale = 1.0f;
        }
    }

    private void EnableGlowAndBlur(){
        glowEffect.enabled = true;
        motionBlurEffect.enabled = true;
    }

    private void DisableGlowAndBlur(){
        glowEffect.enabled = false;
        motionBlurEffect.enabled = false;
    }

    private void StopSlowMotion(){
        DisableGlowAndBlur();
        Time.timeScale = 1.0f;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
        movement.SetSlowMotionSpeeds(4,80);
        inSlowMo.Stop();
        speedUpAudio.Play();
    }
    
    //Sets whether it is the selected spell 
    private void CheckIfCurrentSpell(){
        selectedSpellNum = changeSpell.GetSelectedSpellNumber(); 
        
        if(selectedSpellNum == SPELL_NUMBER)
            isSelectedSpell = true;
        else
            isSelectedSpell = false;
    }
}
