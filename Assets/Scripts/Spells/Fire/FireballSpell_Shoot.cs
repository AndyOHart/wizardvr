﻿using UnityEngine;
using System.Collections;

//Handles shooting a fireball
public class FireballSpell_Shoot : MonoBehaviour {

    public GameObject fireballDestroyPrefab;
    private float speed = 9.0f;
    private Vector3 direction;
    private bool noCollision;

    private const float PULSE_RANGE = 6.0f;
    private const float PULSE_SPEED = 3.5f;
    private const float PULSE_MINIMUM = 2.0f; 
    private const float DESTROY_TIME = 1f;

    public AudioSource fireballHit;
    
    void Start () {     
        direction = transform.forward;  
        noCollision = true; 
       
    }

    void Update () { 
        this.light.range = PULSE_MINIMUM + Mathf.PingPong(Time.time * PULSE_SPEED, PULSE_RANGE - PULSE_MINIMUM);
        
        if(noCollision){
            transform.position += (speed * Time.deltaTime) * direction; 
        }
        else{
            gameObject.rigidbody.useGravity = false;
            gameObject.rigidbody.isKinematic = true;
        }

    }

    //Lights a torch if it has been collided with
    private void LightTorch(Collision collision, Torch torch){
        //Torch torchScript = collision.collider.gameObject.GetComponent<Torch>();  
        torch.EnableFlame();
    }

    private void DealDamage(Collision collision, Health health){
        HurtPlayer hp = new HurtPlayer();
        hp.DealDamage(health, 20f);
    }

    //When it colliders with something, destroy fireball after destroy time
    void OnCollisionEnter(Collision collision){    
        noCollision = false; 

        if(collision.collider.gameObject.GetComponent<Torch>())
            LightTorch(collision, collision.collider.gameObject.GetComponent<Torch>());

        if(collision.collider.gameObject.GetComponent<Health>())
            DealDamage(collision, collision.collider.gameObject.GetComponent<Health>());

        if(collision.collider.gameObject.name == "DoorPlank")
            collision.collider.gameObject.rigidbody.isKinematic = false;


		Physics.IgnoreCollision(collider, collision.collider);

        GameObject fireBallDestroy = Instantiate(fireballDestroyPrefab.gameObject, this.transform.position, this.transform.rotation) as GameObject;
        Destroy(this.gameObject);         
    }
}
