﻿using UnityEngine;
using System.Collections;

//This class simply destroys the instantiated FireballDestroy_Prefab
public class Fireball_Destroy : MonoBehaviour {

    private const float DESTROY_TIME = 0.40f;

	void Start () {
        Destroy(this.gameObject,DESTROY_TIME);
	}
}
