﻿using UnityEngine;
using System.Collections;

//Used for starting up and handling collisions with the flame thrower
public class FlamethrowerCollisions : MonoBehaviour {

    void OnParticleCollision(GameObject gameObj){
         
        //If the particles collide with a torch, light it up
        if(gameObj.name == "Torch" || gameObj.name == "FloorTorch"|| gameObj.name == "Chandelier"
           || gameObj.name == "BowlLight" || gameObj.name == "WallTorch"){
            Torch torch = gameObj.GetComponent<Torch>();
            torch.EnableFlame();
        }
       
        if(gameObj.GetComponent<Health>()){
            DealDamage(gameObj.GetComponent<Health>());
        }
    }

    private void DealDamage(Health health){
        HurtPlayer hp = new HurtPlayer();
        hp.DealDamage(health, 20f);
    }
}
