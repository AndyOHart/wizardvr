﻿using UnityEngine;
using System.Collections;

//Class handling the fireball spell
public class FireSpell : Spell {

    //Public instances to get in the inspector
    public AudioSource FireShoot;
    public AudioSource CantShoot;
    public AudioSource FlamethrowerAudio;

    private GameObject FireballPrefab;
    private bool FreeToShoot;

    //For shooting a fireball
    private float FireRate = 2.0f;
    private float LastShot = 0.0f;

    //Sets up the default values
	void Awake () {
        FireballPrefab = Resources.Load("Fireball/Fireball") as GameObject;
        FreeToShoot = true;
        TriggerIsPressed = false;

	}

	protected override void OnUpdate () {

        //If trigger is being pressed in, enable the flamethrower emissions
        if (SixenseInput.Controllers [1].Trigger > 0.0){
            ChangeSpell.SetIsBusy(true);
            FlamethrowerParticleSystem.enableEmission = true;

            if(!FlamethrowerParticleSystem.isPlaying)
                FlamethrowerParticleSystem.Play();

            if(!FlamethrowerAudio.isPlaying)
                FlamethrowerAudio.Play();
        }
        else{
            ChangeSpell.SetIsBusy(false);
            FlamethrowerParticleSystem.enableEmission = false;
            FlamethrowerAudio.Stop();
        }

        //When the bumper is pressed, shoot a fireball
        if (SixenseInput.Controllers[1].GetButtonDown (SixenseButtons.BUMPER) && TriggerIsPressed == false && FreeToShoot) {
            Shoot();
        }
	}

    //Handles shooting the fireball, instantiates it, and issues a cooldown
    protected override void Shoot(){
        if(Time.time > FireRate + LastShot){
            FireShoot.Play();
            GameObject fireball = Instantiate(FireballPrefab.gameObject, SpellEmitter.transform.position, SpellEmitter.transform.rotation) as GameObject;
            LastShot = Time.time;
        }
        else
            CantShoot.Play();
    }
}
