using UnityEngine;
using System.Collections;

public class PhysicsSpell : Spell {

    public AudioSource ObjectLiftedAudio;
    public AudioSource ShootAudio;
	public AudioSource PushAudio;
    public AudioSource ReleaseItemAudio;

    private enum PhysicsSpellState { Free, Catch, Occupied, Charge, Release, Push};
    private PhysicsSpellState CurrentPhysicsSpellState;
    private Rigidbody Rigid;
    private float CurrentForce;
    private float TimeToLerp = 4;
    private float LerpTimer = 0f;

    private const float MAX_FORCE = 1000.0f;
    private const int MIN_FORCE= 0;
    private const float CATCH_RANGE= 30.0f;
    private const float HOLD_DISTANCE= 2.0f;
    private const int LAYER_MASK = -1;

    private bool InHeldState;
    private Vector3 OriginalPos;
    private float Timer;
    private float Cooldown;
    private bool WasClose;

    //On start, set state to free and set up other variabels
    void Awake(){
        CurrentPhysicsSpellState = PhysicsSpellState.Free;
        Rigid = null;
        CurrentForce = MIN_FORCE;
		InHeldState = false;
        WasClose = false;
    }    
    
    //FixedUpdate means it is constantly waiting to enter a state and once it does it works in a fixed way
    void FixedUpdate(){
        switch( CurrentPhysicsSpellState ){
            case PhysicsSpellState.Free : Free(InHeldState); break; //If not holding anything
            case PhysicsSpellState.Catch : Catch (); break;         //Once in catch state, move the object to the player, go to Occupied state
            case PhysicsSpellState.Occupied : Occupied(); break;    //When occupied, keep object in front of player, checks if bumper or trigger is pressed. If trigger, go to Charge state, else if Bumper go to Release state
            case PhysicsSpellState.Charge : Charge(); break;        //Used to charge up how powerful you want the shot to be, using Hydra trigger values
            case PhysicsSpellState.Release : Release(); break;      //Sets object to use gravity again, add a force to object so it is shot and resets the values for the charge and enters Free state
        }
    }
    
    private void Free(bool inHeldState)
    {
        ChangeSpell.SetIsBusy(false);

        //If trigger pressed when free, initiate the push
        if(inHeldState == false && (SixenseInput.Controllers[1].Trigger > 0.0f ) && !(SixenseInput.Controllers[1].GetButtonDown(SixenseButtons.BUMPER))){
			ForcePush(inHeldState, 100);

            PushEmitter.transform.position = SpellEmitter.transform.position;
            PushEmitter.enableEmission = true;

            ChangeSpell.SetIsBusy(true);
        }
		else{
			PushAudio.Stop();
            PushEmitter.enableEmission = false;
            ChangeSpell.SetIsBusy(false);
		}

        //Send out a raycast, if it hits object, enter catch state. Disables gravity, otherwise object slowly floats downwards
        if(SixenseInput.Controllers[1].GetButtonDown(SixenseButtons.BUMPER) && !(SixenseInput.Controllers[1].GetButtonDown(SixenseButtons.TRIGGER))){

            //sends out the ray
            Vector3 rayDirection = SpellEmitter.transform.TransformDirection(Vector3.forward);
			Vector3 rayStart = SpellEmitter.transform.position + rayDirection;
            var hit = new RaycastHit();

            //If raycast has hit something
            if(Physics.Raycast(SpellEmitter.transform.position, SpellEmitter.transform.forward, out hit, CATCH_RANGE, LAYER_MASK)){
                  
                //if the mass of the object is less than 50, allow pickup, enter catch state
				if(hit.rigidbody && hit.rigidbody.mass < 50){
                    Rigid = hit.rigidbody;
					Rigid.isKinematic = false;
                    Rigid.useGravity = false;

                    OriginalPos = Rigid.transform.position;
                    Timer = 0f;
                    ObjectLiftedAudio.mute = false;
                    ObjectLiftedAudio.Play();

                    if((SpellEmitter.transform.position - Rigid.gameObject.transform.position).magnitude < 3f){
                        WasClose = true;
                        CurrentPhysicsSpellState = PhysicsSpellState.Occupied;
                    }
                    else
                        CurrentPhysicsSpellState = PhysicsSpellState.Catch;
                }
            }else
                ReleaseItemAudio.Play();
        } 
    }

    //object has been caught and will move towards the player
    private void Catch()
    {
        InHeldState = true;
        ChangeSpell.SetIsBusy(true);
        Vector3 targetPosition = SpellEmitter.transform.position + SpellEmitter.transform.forward * HOLD_DISTANCE;
        EnablePhysicsBeam();

        //moves the object towards the player over a timer
        Rigid.MovePosition(Vector3.Lerp(Rigid.transform.position, targetPosition, Time.deltaTime / 0.2f));

        if(Vector3.Distance(Rigid.transform.position,targetPosition) < 0.5f)
            CurrentPhysicsSpellState = PhysicsSpellState.Occupied;
    }

    //the object is currently being held
    private void Occupied()
    {
        //move the object here the wand is moving
        if(WasClose){
            EnablePhysicsBeam();
            InHeldState = true;
            ChangeSpell.SetIsBusy(true);
            Rigid.MovePosition((Vector3.Lerp(Rigid.transform.position, SpellEmitter.transform.position + SpellEmitter.transform.forward, Time.deltaTime / 0.2f)));
        }
        else
            Rigid.MovePosition(SpellEmitter.transform.position + SpellEmitter.transform.forward * HOLD_DISTANCE);

        //if the trigger is pressed, charge, otherwise, release
        if(SixenseInput.Controllers[1].Trigger != 0.0)
            CurrentPhysicsSpellState = PhysicsSpellState.Charge;
        else if(SixenseInput.Controllers[1].GetButtonUp(SixenseButtons.BUMPER))
            CurrentPhysicsSpellState = PhysicsSpellState.Release;
    }

    //trigger is being held and the charge has initialized
    private void Charge()
    {
        //keep object following the wand
        if(WasClose)
            Rigid.MovePosition((SpellEmitter.transform.position + SpellEmitter.transform.forward));
        else
            Rigid.MovePosition(SpellEmitter.transform.position + SpellEmitter.transform.forward * HOLD_DISTANCE);

        //set force and audio pitch
        if (SixenseInput.Controllers [1].Trigger != 0 && (SixenseInput.Controllers[1].Trigger * 2.0f) > 0.5f){
            CurrentForce = (float)(SixenseInput.Controllers[1].Trigger) * MAX_FORCE;
            ObjectLiftedAudio.pitch = (float)(SixenseInput.Controllers[1].Trigger) * 3f;
        }  
        //else if the trigger is released, go back to occupied state
        else {
            ObjectLiftedAudio.pitch = 1;
            ObjectLiftedAudio.mute = false;
            CurrentForce = 0;
            CurrentPhysicsSpellState = PhysicsSpellState.Occupied;
        }

        //when bumper is pressed, release the object
        if(SixenseInput.Controllers[1].GetButtonUp(SixenseButtons.BUMPER)){
            CurrentPhysicsSpellState = PhysicsSpellState.Release;  
        }
    }

    //handles releasing the object
    private void Release()
    {
 
        if(CurrentForce > MIN_FORCE)
            ShootAudio.Play();
        else
            ReleaseItemAudio.Play();

        //using the current force of the object, it will either drop or shoot
        PhysicsBeamEmitter.Stop();
        PhysicsBeamEmitter.enableEmission = false;

        ObjectLiftedAudio.Stop();
        ObjectLiftedAudio.mute = true;
        ObjectLiftedAudio.pitch = 1;
        Rigid.useGravity = true;
        Rigid.AddForce(SpellEmitter.transform.forward * CurrentForce);
        Rigid.velocity = new Vector3(0,0,0);

        CurrentForce = MIN_FORCE;
        InHeldState = false;
        Cooldown = Time.time + 2.0f;
        WasClose = false;
        CurrentPhysicsSpellState = PhysicsSpellState.Free;
    }

    //handles pushing objects
	private void ForcePush(bool inHeldState, float forceAmount){

        //cooldown is after an object is shot, so it doesnt push the object straight away
        if(Cooldown <= Time.time){
            if(inHeldState == false && SixenseInput.Controllers[1].Trigger > 0.0){

                //uses raycast to find object
                Vector3 forward = SpellEmitter.transform.TransformDirection(Vector3.forward);
                Vector3 forceDirection;
                RaycastHit hit = new RaycastHit();

                if(!PushAudio.isPlaying)
                    PushAudio.Play();

                if(!PushEmitter.isPlaying)
                    PushEmitter.Play();

                //when it collides with an object that has a rigidbody
                if(Physics.Raycast(SpellEmitter.transform.position, forward, out hit, 10) && hit.rigidbody != null){
                    forceDirection = hit.transform.position - SpellEmitter.transform.position;
                
                    //depending on the weight, apply a force
					if(hit.rigidbody.mass < 50)
                    	hit.rigidbody.AddForceAtPosition(forceDirection.normalized * forceAmount, hit.transform.position);
               		else
						hit.rigidbody.AddForceAtPosition(forceDirection.normalized * forceAmount * 4, hit.transform.position);
				
                    if(hit.collider.gameObject.name == "WoodenDoor")
                        hit.rigidbody.isKinematic = false;
                }
            }
        }
    }

    private void EnablePhysicsBeam(){
        PhysicsBeamEmitter.transform.position = SpellEmitter.transform.position;
        PhysicsBeamEmitter.enableEmission = true;
        
        if(!PhysicsBeamEmitter.isPlaying)
            PhysicsBeamEmitter.Play();

    }
}