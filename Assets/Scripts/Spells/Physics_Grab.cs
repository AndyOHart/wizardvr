﻿
using UnityEngine;
using System.Collections;

public class Physics_Grab : MonoBehaviour {
    
    public float catchRange= 30.0f;
    public float holdDistance= 4.0f;
    public int layerMask = -1;
    public Light currentSpellLight;
    public ChangeSpell changespell;
    public GameObject spellEmitter;

    private int selectedSpellNum;
    private bool isSelectedSpell;
    private bool triggerIsPressed;
    
    enum PhysicsSpellState { Free, Catch, Occupied, Charge, Release, Push};
    private PhysicsSpellState physicsSpellState;
    private Rigidbody rigid;
    private float currentForce;
    private float TimeToLerp = 4;
    private float lerpTimer = 0f;
    
    private const int SPELL_NUMBER = 2;
    private const float MAX_FORCE = 1000.0f;
    private const int minForce= 0;
    private bool inHeldState = false;
    Vector3 originalPos;
    float timer;
    
    
    //Sets things up
    void Start(){
        physicsSpellState = PhysicsSpellState.Free;
        rigid = null;
        currentForce = minForce;
        isSelectedSpell = false;
    }
    
    
    
    //Checks for if spell is changed etc
    void Update(){
        CheckIfCurrentSpell();
        SetBusy();
    }
    

    
    //FixedUpdate means it is constantly waiting to enter a state and once it does it works in a fixed way
    void FixedUpdate(){
        
        switch( physicsSpellState )
        {
            case PhysicsSpellState.Free : Free(inHeldState); break;            //If not holding anything
            case PhysicsSpellState.Catch : Catch (); break;         //Once in catch state, move the object to the player, go to Occupied state
            case PhysicsSpellState.Occupied : Occupied(); break;    //When occupied, keep object in front of player, checks if bumper or trigger is pressed. If trigger, go to Charge state, else if Bumper go to Release state
            case PhysicsSpellState.Charge : Charge(); break;        //Used to charge up how powerful you want the shot to be, using Hydra trigger values
            case PhysicsSpellState.Release : Release(); break;      //Sets object to use gravity again, add a force to object so it is shot and resets the values for the charge and enters Free state
        }
    }
    
    private void Free(bool inHeldState)
    {
        //If trigger pressed when free
        if(isSelectedSpell && inHeldState == false && (SixenseInput.Controllers[1].Trigger > 0.0f ) && !(SixenseInput.Controllers[1].GetButtonDown(SixenseButtons.BUMPER))){
            ForcePush(inHeldState);
        }

        //Send out a raycast, if it hits object, enter catch state. Disables gravity, otherwise object slowly floats downwards
        if(isSelectedSpell && (SixenseInput.Controllers[1].GetButtonDown(SixenseButtons.BUMPER)) && !(SixenseInput.Controllers[1].GetButtonDown(SixenseButtons.TRIGGER))){
            
            var hit = new RaycastHit();
            if(Physics.Raycast(spellEmitter.transform.position, spellEmitter.transform.forward, out hit, catchRange, layerMask)){
                
                if(hit.rigidbody){
                    Debug.Log("Hit");
                    rigid = hit.rigidbody;
                    rigid.useGravity = false;
                    
                    originalPos = rigid.transform.position;
                    timer = 0f;
                    
                    physicsSpellState = PhysicsSpellState.Catch;
                }
            }
        }
    }
    
    private void Catch()
    {
        Debug.Log("Catch");
        Vector3 targetPosition = spellEmitter.transform.position + spellEmitter.transform.forward * holdDistance;
        rigid.MovePosition(Vector3.Lerp(rigid.transform.position, targetPosition, Time.deltaTime / 0.2f));
        inHeldState = true;

        if(Vector3.Distance(rigid.transform.position,targetPosition) < 1.0f)
            physicsSpellState = PhysicsSpellState.Occupied;
    }
    
    private void Occupied()
    {
        Debug.Log("Occupied");
        rigid.MovePosition(spellEmitter.transform.position + spellEmitter.transform.forward * holdDistance);
        
        if(isSelectedSpell && SixenseInput.Controllers[1].Trigger != 0.0)
            physicsSpellState = PhysicsSpellState.Charge;
        else if(isSelectedSpell && SixenseInput.Controllers[1].GetButtonDown(SixenseButtons.BUMPER))
            physicsSpellState = PhysicsSpellState.Release;
    }
    
    private void Charge()
    {
        Debug.Log("Charge");
        rigid.MovePosition(spellEmitter.transform.position + spellEmitter.transform.forward * holdDistance);

        if (isSelectedSpell && SixenseInput.Controllers [1].Trigger != 0 && (SixenseInput.Controllers[1].Trigger * 2.0f) > 0.5f){
            currentForce = (float)(SixenseInput.Controllers[1].Trigger) * MAX_FORCE;
        }  
        else {
            currentForce = 0;
            physicsSpellState = PhysicsSpellState.Occupied;
        }
        
        if(isSelectedSpell && SixenseInput.Controllers[1].GetButtonDown(SixenseButtons.BUMPER))
            physicsSpellState = PhysicsSpellState.Release;
    }
    
    private void Release()
    {
        Debug.Log("released");
        rigid.useGravity = true;
        rigid.AddForce(spellEmitter.transform.forward * currentForce);
        currentForce = minForce;
        inHeldState = false;
        physicsSpellState = PhysicsSpellState.Free;
    }
    
    //Sets whether it is the selected spell
    private void CheckIfCurrentSpell(){
        
        selectedSpellNum = changespell.GetSelectedSpellNumber();
        if(selectedSpellNum == SPELL_NUMBER)
            isSelectedSpell = true;
        else
            isSelectedSpell = false;
    }
    
    private void SetBusy(){
        
        if(physicsSpellState != PhysicsSpellState.Free)
            changespell.SetIsBusy(true);
        else
            changespell.SetIsBusy(false);
    }

    private void ForcePush(bool inHeldState){

        if(inHeldState == false && SixenseInput.Controllers[1].Trigger == 0.0){
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            Vector3 forceDirection;
            RaycastHit hit = new RaycastHit();
            
            if(Physics.Raycast(spellEmitter.transform.position, forward, out hit, 10) && hit.rigidbody != null){
                forceDirection = hit.transform.position - spellEmitter.transform.position;
                hit.rigidbody.AddForceAtPosition(forceDirection.normalized * 100, hit.transform.position);
            }
        }
    }
}