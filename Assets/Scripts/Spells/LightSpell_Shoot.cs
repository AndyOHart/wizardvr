﻿using UnityEngine;
using System.Collections;

public class LightSpell_Shoot : MonoBehaviour {
 
    private float speed = 7.0f;
    private Vector3 direction;
    private bool noCollision;
      
    private const float PULSE_RANGE = 6.0f;
    private const float PULSE_SPEED = 3.5f;
    private const float PULSE_MINIMUM = 2.0f; 
    private const int DESTROY_TIME = 10;

    void Start () {     
        direction = transform.forward;  
        noCollision = true;      
    }
    
    // Update is called once per frame
    void Update () { 
        this.light.range = PULSE_MINIMUM + Mathf.PingPong(Time.time * PULSE_SPEED, PULSE_RANGE - PULSE_MINIMUM);

        if(noCollision){
            transform.position += (speed * Time.deltaTime) * direction; 
        }
    }

    //When it colliders with something, destroy light after destroy time
    void OnCollisionEnter(Collision collision){ 
        Destroy(this.gameObject,DESTROY_TIME);
        noCollision = false;     
    }
}
