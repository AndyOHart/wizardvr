﻿using UnityEngine;
using System.Collections;

public class Fireball_Spell : MonoBehaviour {

    //Public instances to get in the inspector
    public Light currentSpellLight;
    public GameObject fireballPrefab;
    public GameObject spellEmitter;
    public ChangeSpell changeSpell;

    //Spell selection stuff
    private int selectedSpellNum;
    private bool isSelectedSpell;
    private bool triggerIsPressed;
    private bool freeToShoot;

    //Constants
    private const int SPELL_NUMBER = 1;

    //For shooting a fireball
    private float fireRate = 2.0f;
    private float lastShot = 0.0f;

	// Use this for initialization
	void Start () {
        freeToShoot = true;
        isSelectedSpell = false;
        triggerIsPressed = false;
	}
	
	// Update is called once per frame
	void Update () {
        CheckIfCurrentSpell();

        //What happens when bumper is pressed
        if (isSelectedSpell && SixenseInput.Controllers[1].GetButtonDown (SixenseButtons.BUMPER) && triggerIsPressed == false && freeToShoot) {
            ShootFireBall();
        }
	}

    //Sets whether it is the selected spell 
    private void CheckIfCurrentSpell(){
        selectedSpellNum = changeSpell.GetSelectedSpellNumber(); 

        if(selectedSpellNum == SPELL_NUMBER)
            isSelectedSpell = true;
        else
            isSelectedSpell = false;
    }

    //Handles shooting the light
    private void ShootFireBall(){
        if(Time.time > fireRate + lastShot){
            GameObject fireball = Instantiate(fireballPrefab.gameObject, spellEmitter.transform.position, spellEmitter.transform.rotation) as GameObject;
            lastShot = Time.time;
        }
    }
}
