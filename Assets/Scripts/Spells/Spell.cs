﻿using UnityEngine;
using System.Collections;

public class Spell:MonoBehaviour{

    public PlayerData PlayerData;
    public GameObject SpellEmitter;
    public ChangeSpell ChangeSpell;
    public Light CurrentSpellLight;
    public bool TriggerIsPressed;
    public ParticleSystem HealEmitter;
    public ParticleSystem PhysicsBeamEmitter;
    public ParticleSystem PushEmitter;
    public ParticleSystem FlamethrowerParticleSystem;

    void Start(){
        CurrentSpellLight = gameObject.transform.FindChild("CurrentSpellLight").light;
        SpellEmitter = gameObject.transform.FindChild("SpellEmitter").gameObject;
        HealEmitter = SpellEmitter.gameObject.transform.FindChild("HealEmitter").particleSystem;
        FlamethrowerParticleSystem = SpellEmitter.gameObject.transform.FindChild("FlamethrowerEmitter").particleSystem;
        PhysicsBeamEmitter = SpellEmitter.gameObject.transform.FindChild("PhysicsBeam").particleSystem;
        PushEmitter = SpellEmitter.gameObject.transform.FindChild("PushEmitter").particleSystem;
        ChangeSpell = gameObject.GetComponent<ChangeSpell>();
        PlayerData = gameObject.GetComponent<PlayerData>();

        OnAwake();
    }

    protected virtual void OnAwake(){}

    void Update(){
        OnUpdate();
    }

    protected virtual void OnUpdate(){}

    protected virtual void Shoot(){}
}
