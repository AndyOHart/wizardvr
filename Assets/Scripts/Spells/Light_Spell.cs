using UnityEngine;
using System.Collections;

public class Light_Spell : MonoBehaviour {

    //Public instances to get in the inspector
    public Light currentSpellLight;
    public GameObject wandLightProjectilePrefab;
    public GameObject spellEmitter;
    public ChangeSpell changeSpell;
    public GameObject wandTip;

    //Spell selection stuff
    private int selectedSpellNum;
    private bool isSelectedSpell;
	private bool triggerIsPressed;

    //Constants
    private const int SPELL_NUMBER = 3;
    private const float LIGHT_INTENSITY = 1.0f;
    private const float TRIGGER_LIGHT_RANGE_MIN = 3.0f;
    private const float PULSE_RANGE = 8.0f;
    private const float PULSE_SPEED = 2.0f;
    private const float PULSE_MINIMUM = 5.0f;

    //For shooting a light
    private float cooldown;

    private LensFlare flare;

	// Find the light object and disable it
	void Start () {
        isSelectedSpell = true;
		triggerIsPressed = false;
        audio.playOnAwake = true;
        currentSpellLight.intensity = LIGHT_INTENSITY;
        flare = wandTip.GetComponent<LensFlare>();
        flare.brightness = 0;
	}
	
	// Handles button presses etc
	void Update () {
   
        CheckIfCurrentSpell();
       
        //What happens when trigger is pressed
        if (isSelectedSpell && SixenseInput.Controllers [1].Trigger != 0 && (SixenseInput.Controllers[1].Trigger * 2.0f) > 1.00f){
			triggerIsPressed = true;
            audio.enabled = true;
            audio.volume = (float)(SixenseInput.Controllers[1].Trigger-0.600);
            currentSpellLight.range =  (float)(SixenseInput.Controllers[1].Trigger) * (PULSE_MINIMUM + Mathf.PingPong(Time.time * PULSE_SPEED, PULSE_RANGE - PULSE_MINIMUM));
            flare.brightness = (float)(SixenseInput.Controllers[1].Trigger) * (0.70f + Mathf.PingPong(Time.time * 0.2f, 1 - 0.8f));
		}  
        //When trigger is released reset values
        else {
			triggerIsPressed = false;
            audio.enabled = false;
            currentSpellLight.range = TRIGGER_LIGHT_RANGE_MIN;
            flare.brightness = 0;
		}

        //What happens when bumper is pressed
        if (isSelectedSpell && SixenseInput.Controllers[1].GetButtonDown (SixenseButtons.BUMPER) && triggerIsPressed == false) {
            Debug.Log("Shoot Check");
            ShootLight();

		}
	}

    //Handles shooting the light
    private void ShootLight(){
        if(cooldown <= Time.time){
            Debug.Log("Shot");
            GameObject lightShot = Instantiate(wandLightProjectilePrefab.gameObject, spellEmitter.transform.position, spellEmitter.transform.rotation) as GameObject;
            cooldown = Time.time + 10.0f;
        }
    }

    //Sets whether it is the selected spell 
    private void CheckIfCurrentSpell(){

        selectedSpellNum = changeSpell.GetSelectedSpellNumber(); 
        if(selectedSpellNum == SPELL_NUMBER)
            isSelectedSpell = true;
        else
            isSelectedSpell = false;
    }

}




