﻿using UnityEngine;
using System.Collections;

public class FireballSpell_Shoot : MonoBehaviour {

    private float speed = 9.0f;
    private Vector3 direction;
    private bool noCollision;
    
    private const float PULSE_RANGE = 6.0f;
    private const float PULSE_SPEED = 3.5f;
    private const float PULSE_MINIMUM = 2.0f; 

    public AudioSource audio_fireballHit;
    
    void Start () {     
        //var audioSources = GetComponents(AudioSource);
        //audio_fireballHit = audioSources[1];
        direction = transform.forward;  
        noCollision = true; 
       
    }
    
    // Update is called once per frame
    void Update () { 
        this.light.range = PULSE_MINIMUM + Mathf.PingPong(Time.time * PULSE_SPEED, PULSE_RANGE - PULSE_MINIMUM);
        
        if(noCollision){
            transform.position += (speed * Time.deltaTime) * direction; 
        }
    }

    //When it colliders with something, destroy fireball after destroy time
    void OnCollisionEnter(Collision collision){ 

        //audio_fireballHit.Play();

        //Check if it is a torch
        Torch torchScript = collision.collider.gameObject.GetComponent<Torch>();

        if(torchScript != null){
           
            torchScript.EnableFlame();
        }

        Destroy(this.gameObject);
        noCollision = false;     
    }
}
