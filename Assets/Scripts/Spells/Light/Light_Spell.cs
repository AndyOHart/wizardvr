using UnityEngine;
using System.Collections;

public class Light_Spell : Spell {

    //Public instances to get in the inspector
    public GameObject WandLightProjectilePrefab;
    public AudioSource TriggerAudio;
    public AudioSource NoPower;
    public AudioSource ShootLightAudio;

    //Constants
    private const float LIGHT_INTENSITY = 1.0f;
    private const float TRIGGER_LIGHT_RANGE_MIN = 3.0f;
    private const float PULSE_RANGE = 13.0f;
    private const float PULSE_SPEED = 2.0f;
    private const float PULSE_MINIMUM = 5.0f;

    //For the countdown timer of the trigger
    private float TriggerTimer;
    private float TargetTime = 5f;
    private float RegenPerSecond = 0.3f;
    private float DelayBeforeRegen = 1;
    private float LightDepletionPerSecond = 1;
    private float LastUsageTime = -1;
    private float LightShotCooldown;

	// On starting, set up variables
	void OnAwake () {  
		TriggerIsPressed = false;
        CurrentSpellLight.intensity = LIGHT_INTENSITY;
        TriggerAudio.playOnAwake = false;

	}
	
    protected override void OnUpdate(){
        //When trigger is pressed, use the pressure value to issue an amount of light range
        if (SixenseInput.Controllers [1].Trigger != 0 && (SixenseInput.Controllers[1].Trigger * 2.0f) > 1.00f && PlayerData.GetHasPower()){
            ChangeSpell.SetIsBusy(true);

            TriggerAudio.enabled = true;
            TriggerAudio.mute = false;
            TriggerAudio.volume = (float)(SixenseInput.Controllers[1].Trigger-0.600);
            CurrentSpellLight.range =  (float)(SixenseInput.Controllers[1].Trigger) * (PULSE_MINIMUM + Mathf.PingPong(Time.time * PULSE_SPEED, PULSE_RANGE - PULSE_MINIMUM));

            LastUsageTime = Time.time;

            //If you have power, start depleting it
            if(PlayerData.LightPower > 0)
                PlayerData.LightPower -= Time.deltaTime * LightDepletionPerSecond; 
            else {
                PlayerData.SetHasPower(false);
                NoPower.Play();
            }
        }
        //When the trigger is released, start to recharge the light power
        else {
            ChangeSpell.SetIsBusy(false);

            if(Time.time > LastUsageTime + DelayBeforeRegen && !(PlayerData.LightPower > PlayerData.MAX_LIGHT_POWER)){
                PlayerData.LightPower = (PlayerData.LightPower < PlayerData.MAX_LIGHT_POWER) ? (PlayerData.LightPower + Time.deltaTime * RegenPerSecond) : PlayerData.MAX_LIGHT_POWER;
                PlayerData.SetHasPower(true);
            }   

			TriggerIsPressed = false;
            TriggerAudio.enabled = false;
            CurrentSpellLight.range = TRIGGER_LIGHT_RANGE_MIN;
		}

        //When the bumper is pressed, shoot the light
        if (SixenseInput.Controllers[1].GetButtonDown (SixenseButtons.BUMPER) && TriggerIsPressed == false)
            Shoot();
	}


    //When the light is shot, instantiate a projectile of a light and initiate a 10 second cooldown
    protected override void Shoot(){
        if(LightShotCooldown <= Time.time){
            GameObject lightShot = Instantiate(WandLightProjectilePrefab.gameObject, SpellEmitter.transform.position, SpellEmitter.transform.rotation) as GameObject;
            LightShotCooldown = Time.time + 10.0f;
            ShootLightAudio.Play();
        }
        else{
            NoPower.Play();
        }
    }
}