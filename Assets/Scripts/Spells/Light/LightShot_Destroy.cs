﻿using UnityEngine;
using System.Collections;

public class LightShot_Destroy : MonoBehaviour {

    private const float DESTROY_TIME = 1f;
    private float startTime;
    private float endTime;
    private Light light;

    //Once woken started initialize the timer and end timer
    private void Awake(){
        startTime = Time.time;
        endTime = startTime + DESTROY_TIME;
    }

    //Sets the light
    public void SetLight(Light l){
        light = l;
    }

    //When started, destroy after a time
	void Start () {
        Destroy(this.gameObject,DESTROY_TIME);
	}
	
	//Change intensity over time
	void Update () {
	    if(gameObject.light)
            gameObject.light.intensity = Mathf.InverseLerp(endTime, startTime, Time.time);
	}
}
