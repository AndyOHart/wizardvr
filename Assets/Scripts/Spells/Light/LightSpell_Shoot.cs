﻿using UnityEngine;
using System.Collections;

public class LightSpell_Shoot : MonoBehaviour {
 
    private float speed = 7.0f;
    private Vector3 direction;
    private bool noCollision;
      
    private const float PULSE_RANGE = 10.0f;
    private const float PULSE_SPEED = 3.5f;
    private const float PULSE_MINIMUM = 2.0f; 
    private const int DESTROY_TIME = 10;

    public GameObject lightDestroy;

    //When started, set the direction and collision
    void Start () {     
        direction = transform.forward;  
        noCollision = true;      
    }
    
    //Pulse the light and while it hasn't collided, move it through the world
    void Update () { 
        this.light.range = PULSE_MINIMUM + Mathf.PingPong(Time.time * PULSE_SPEED, PULSE_RANGE - PULSE_MINIMUM);

        if(noCollision){
            transform.position += (speed * Time.deltaTime) * direction; 
        }
    }

    //When collided, stop moving and start CoRoutine to destory
    void OnCollisionEnter(Collision collision){ 

        StartCoroutine(StartDestroyPrefab());
        Destroy(this.gameObject,DESTROY_TIME);
        rigidbody.isKinematic = true;
        noCollision = false;     
    }

    //Waits 9.9 seconds then instantiates a destory light
    IEnumerator StartDestroyPrefab(){
        yield return new WaitForSeconds(9.9f);
        lightDestroy = Instantiate(lightDestroy.gameObject, this.transform.position, this.transform.rotation) as GameObject;
        lightDestroy.SendMessage("SetLight", gameObject.light);
    }
}
