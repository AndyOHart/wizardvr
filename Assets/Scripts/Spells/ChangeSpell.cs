﻿using UnityEngine;
using System.Collections;

//Handles changing spells
public class ChangeSpell : MonoBehaviour {

    //Public variables for audio, particle system and the light
    public Light SpellLight;
    public ParticleSystem WandTip;
    private AudioSource SpellAudio;
    public AudioSource ChangeFireAudio;
    public AudioSource ChangeLightAudio;
    public AudioSource ChangePhysicsAudio;
    public AudioSource ChangeLifeSaverAudio;

    //Colours
    private Color lightSpellColour = new Color(0.953f,0.929f,0.167f);
    private Color fireSpellColour = new Color(1.0f,0.443f,0.047f);
    private Color physicsSpellColour = new Color(0.090f,0.462f,0.729f);
    private Color lifesaverSpellColour = new Color(0.047f,1.0f,0.258f);

    //Constants for buttons
    private const int BUTTON_ONE = 1;
    private const int BUTTON_TWO = 2;
    private const int BUTTON_THREE = 3;
    private const int BUTTON_FOUR = 4;

    //Spell Scripts
    private Light_Spell LightSpellScript;
    private FireSpell FireSpellScript;
    private PhysicsSpell PhysicsSpellScript;
    private LifeSaver LifeSaverSpellScript;

    private bool isBusy;
    private int selectedButton;

    //Finds the spells and sets the default as the light spell
    void Awake(){
        selectedButton = 3;
        isBusy = false;

        LightSpellScript = gameObject.GetComponent<Light_Spell>();
        FireSpellScript = gameObject.GetComponent<FireSpell>();
        PhysicsSpellScript = gameObject.GetComponent<PhysicsSpell>();
        LifeSaverSpellScript = gameObject.GetComponent<LifeSaver>();

        DisableAllSpells();
        InitializeStartSpell(lightSpellColour, selectedButton);
    }

	void Update () {
        //Light spell selected
        if(SixenseInput.Controllers[1].GetButtonUp(SixenseButtons.THREE) && (selectedButton != BUTTON_THREE) && isBusy == false){
            UpdateSpellState(lightSpellColour, ChangeLightAudio, BUTTON_THREE);
            LightSpellScript.enabled = true;
        }

        //Fire spell selected
        if(SixenseInput.Controllers[1].GetButtonUp(SixenseButtons.ONE) && (selectedButton != BUTTON_ONE) && isBusy == false){
            UpdateSpellState(fireSpellColour, ChangeFireAudio, BUTTON_ONE);
            FireSpellScript.enabled = true;
        }

        //Physics spell selected
        if(SixenseInput.Controllers[1].GetButtonUp(SixenseButtons.TWO) && (selectedButton != BUTTON_TWO) && isBusy == false){
            UpdateSpellState(physicsSpellColour, ChangePhysicsAudio, BUTTON_TWO);
            PhysicsSpellScript.enabled = true;
        }

        //Lightsaver spell selected
        if(SixenseInput.Controllers[1].GetButtonUp(SixenseButtons.FOUR) && (selectedButton != BUTTON_FOUR) && isBusy == false){
            UpdateSpellState(lifesaverSpellColour, ChangeLifeSaverAudio, BUTTON_FOUR);
            LifeSaverSpellScript.enabled = true;
       }
	}

    //Disable all spell scripts
    private void DisableAllSpells(){
        LightSpellScript.enabled = false;
        FireSpellScript.enabled = false;
        PhysicsSpellScript.enabled = false;
        LifeSaverSpellScript.enabled = false;
    }

    //Changes the spell
    private void UpdateSpellState(Color SpellColor, AudioSource SoundFX, int CurrentButton){
        SpellLight.color = WandTip.startColor = SpellColor;
        SoundFX.Play();
        selectedButton = CurrentButton;
        DisableAllSpells();
    }

    private void InitializeStartSpell(Color SpellColor, int CurrentButton){
        SpellLight.color = WandTip.startColor = SpellColor;
        selectedButton = CurrentButton;
        LightSpellScript.enabled = true;
    }

    //If the user is holding down the trigger on a spell, disable changing spells
    public void SetIsBusy(bool ib){
        isBusy = ib;
    }
}
