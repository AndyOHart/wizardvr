﻿using UnityEngine;
using System.Collections;

//This class handles looking at things in the main menu
public class ChoiceMaker : MonoBehaviour {

    private enum MainMenuChoiceState { Free, StartGame, Tutorial, Exit};
    private MainMenuChoiceState MenuState;

    private Transform cameraTransformation = null;
    private float length = 10.0f;
    private float TimeToStare = 2f;
    private float t = 0;
    private bool PlayerLookingAtText;

    private TextMesh StartText;
    private TextMesh TutorialText;
    private TextMesh ExitText;

    private RaycastHit Hit;
    private RaycastHit LastHit;

    private void Start () {
        cameraTransformation = Camera.main.transform;
        PlayerLookingAtText = false;
        MenuState = MainMenuChoiceState.Free;
    }

    private void FixedUpdate(){
        switch( MenuState ){
            case MainMenuChoiceState.Free : Free(Hit); break; 
            case MainMenuChoiceState.StartGame : StartGame (Hit); break;         
            case MainMenuChoiceState.Tutorial : Tutorial(Hit); break;
            case MainMenuChoiceState.Exit : Exit(Hit); break;       
        }
    }

    private void Update(){

        Vector3 rayDirection = cameraTransformation.TransformDirection(Vector3.forward);
        Vector3 rayStart = cameraTransformation.position + rayDirection;
        Debug.DrawRay(rayStart, rayDirection * length, Color.green);

        if(Physics.Raycast(rayStart, rayDirection, out Hit, length)){
        
            if(Hit.collider.name == "StartGameText"){
                MenuState = MainMenuChoiceState.StartGame;
            }
            else if(Hit.collider.name == "TutorialText"){
                MenuState = MainMenuChoiceState.Tutorial;
            }
            else if(Hit.collider.name == "ExitText"){
                MenuState = MainMenuChoiceState.Exit;
            }
            else
                MenuState = MainMenuChoiceState.Free;
        }
    }

    private void Free(RaycastHit hit){
        if(PlayerLookingAtText){
            TextMesh temptext = LastHit.collider.gameObject.GetComponent<TextMesh>();
            ResetText(temptext);
        }
    }

    private void StartGame(RaycastHit hit){
        StartText = Hit.collider.gameObject.GetComponent<TextMesh>();
        TextColorLerp(StartText);
        LastHit = hit;

        if(TimeToStare <= 0.0f)
            Application.LoadLevel("Main");
    }

    private void Tutorial(RaycastHit hit){
        TutorialText = Hit.collider.gameObject.GetComponent<TextMesh>();
        TextColorLerp(TutorialText);
        LastHit = hit;

        if(TimeToStare <= 0.0f)
            Application.LoadLevel("Tutorial");
    }

    private void Exit(RaycastHit hit){
        ExitText = Hit.collider.gameObject.GetComponent<TextMesh>();
        TextColorLerp(ExitText);
        LastHit = hit;
        
        if(TimeToStare <= 0.0f)
            Application.Quit();
    }

    private void TextColorLerp(TextMesh text){
        PlayerLookingAtText = true;
        TimeToStare -= Time.deltaTime;
        text.color = Color.Lerp(Color.white, Color.red, t);
        
        if(t < 1f)
            t += Time.deltaTime / 2f;
    }

    private void ResetText(TextMesh text){
        if(text != null)
            text.color = Color.white;
        PlayerLookingAtText = false;
        t = 0;
        TimeToStare = 2f;
    }
}
