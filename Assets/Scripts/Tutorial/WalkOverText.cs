﻿using UnityEngine;
using System.Collections;

public class WalkOverText : MonoBehaviour {

    public GameObject MoveForwardText;
    public GameObject ChangePhysicsSpellText;
    public GameObject PhysicsPushText;
    public GameObject PhysicsPickupText;
    public GameObject JumpText;
    public GameObject ChangeLightText;
    public GameObject ShootLightText;
    public GameObject TriggerLightText;
    public GameObject OpenChestText;
    public GameObject ChangeFireText;
    public GameObject FlamethrowerText;
    public GameObject FireballText;
    public GameObject ChangeLifesaverText;
    public GameObject HealText;
    public GameObject SlowMotionText;
    public GameObject BoulderWarningText;
    public GameObject EndOfLevelText;

    public Texture2D FadeoutTexture;
    public float FadeSpeed = 2f;
    public int drawDepth = -1000;

    private float alph = 1.0f;
    private int FadeDirection = -1;

    private float WaitTime = 2f;
    private float t = 0;
    private bool Ended = false;

    private void OnGUI(){
        alph += FadeDirection * FadeSpeed * Time.deltaTime;
        alph = Mathf.Clamp01(alph);


        Color c = GUI.color;
        c.a = alph;
        GUI.color = c;

        GUI.depth = drawDepth;
        GUI.DrawTexture(new Rect(0,0,Screen.width, Screen.height), FadeoutTexture);
    }

    private void Update(){
        Debug.Log(WaitTime);

        if(Ended){
            WaitTime -= Time.deltaTime;
            
            if(t < 1f)
                t += Time.deltaTime / 2f;
        }

        if(WaitTime <= 0.0f)
            Application.LoadLevel("MainMenu");
    }

    private void FadeOut(){
        FadeDirection = 1;
        Ended = true;
    }

    private void OnTriggerEnter(Collider collider){

        if(collider.gameObject.name == "MoveForwardText"){
            MoveForwardText.SetActive(false);
            ChangePhysicsSpellText.SetActive(true);
        }

        if(collider.gameObject.name == "ChageSpellPhysicsText"){
            ChangePhysicsSpellText.SetActive(false);
            PhysicsPushText.SetActive(true);
        }

        if(collider.gameObject.name == "PhysicsPushText"){
            PhysicsPushText.SetActive(false);
            PhysicsPickupText.SetActive(true);
        }

        if(collider.gameObject.name == "PhysicsPickupText"){
            PhysicsPickupText.SetActive(false);
            JumpText.SetActive(true);
        }

        if(collider.gameObject.name == "JumpText"){
            JumpText.SetActive(false);
            ChangeLightText.SetActive(true);
        }

        if(collider.gameObject.name == "ChangeLightText"){
            ChangeLightText.SetActive(false);
            ShootLightText.SetActive(true);
        }

        if(collider.gameObject.name == "ShootLightText"){
            ShootLightText.SetActive(false);
            TriggerLightText.SetActive(true);
        }

        if(collider.gameObject.name == "TriggerLightText"){
            TriggerLightText.SetActive(false);
            OpenChestText.SetActive(true);
        }

        if(collider.gameObject.name == "OpenChestText"){
            OpenChestText.SetActive(false);
            ChangeFireText.SetActive(true);
        }

        if(collider.gameObject.name == "ChangeFireText"){
            ChangeFireText.SetActive(false);
            FlamethrowerText.SetActive(true);
        }

        if(collider.gameObject.name == "FlamethrowerText"){
            FlamethrowerText.SetActive(false);
            FireballText.SetActive(true);
        }

        if(collider.gameObject.name == "FireballText"){
            FireballText.SetActive(false);
            ChangeLifesaverText.SetActive(true);
        }

        if(collider.gameObject.name == "ChangeLifesaverText"){
            ChangeLifesaverText.SetActive(false);
            HealText.SetActive(true);
        }

        if(collider.gameObject.name == "HealText"){
            HealText.SetActive(false);
            SlowMotionText.SetActive(true);
        }

        if(collider.gameObject.name == "SlowMotionText"){
            SlowMotionText.SetActive(false);
        }

        if(collider.gameObject.name == "BoulderTrigger"){
            BoulderWarningText.SetActive(true);
        }

        if(collider.gameObject.name == "BoulderWarningText"){
            BoulderWarningText.SetActive(false);
        }

        if(collider.gameObject.name == "EndLevelText"){
            EndOfLevelText.SetActive(false);

            FadeOut();
        }
    }
}
