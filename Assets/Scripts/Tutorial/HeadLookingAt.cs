﻿using UnityEngine;
using System.Collections;

//Use this class for sending raycasts to detect what player looks at

public class HeadLookingAt : MonoBehaviour {
    
    private Transform cameraTransformation = null;
    private WizardCamera wc;
    private float length = 10.0f;
    private int MoveHeadTextCalibrationCheck = 0;

    public OVRCameraController controller;
    public CharacterController cc;
    public GameObject TurnTrigger;
    public TextMesh MoveHeadText;
    public GameObject TurnText;
    public GameObject MoveForwardText;

    
    //Get main camera
    void Start () {
        MoveHeadText.gameObject.SetActive(false);
        cameraTransformation = Camera.main.transform;
        TurnText.SetActive(false);
        MoveForwardText.SetActive(false);
        wc = controller.GetComponent<WizardCamera>();
    }
    
    //Each frame send out raycasts to check what the player is looking at
    void Update () {

        if(wc.HasCalibrated() && MoveHeadTextCalibrationCheck < 1)
            MoveHeadText.gameObject.SetActive(true);

        RaycastHit hit;
        Vector3 rayDirection = cameraTransformation.TransformDirection(Vector3.forward);
        Vector3 rayStart = cameraTransformation.position;

//        Vector3 rayDirection2 = cc.transform.forward;
//        Vector3 rayStart2 = cc.transform.position + rayDirection;
//
//        Debug.DrawRay(rayStart, rayDirection * length, Color.red);
//        Debug.DrawRay(rayStart2, rayDirection2 * length, Color.green);

        if(Physics.Raycast(rayStart, rayDirection, out hit, length)){
            if(hit.collider.name == "MoveHeadTrigger"  && wc.HasCalibrated()){
                MoveHeadText.gameObject.SetActive(false);
                MoveHeadTextCalibrationCheck =1;
                TurnText.SetActive(true);
            }
        }

        if(MoveHeadText.gameObject.activeInHierarchy == false && SixenseInput.Controllers[1].JoystickX >= 0.3f && TurnText.gameObject.activeInHierarchy == true){
            TurnText.SetActive(false);
            MoveForwardText.SetActive(true);
            Destroy(TurnTrigger);
        }
    }
}
