﻿using UnityEngine;
using System.Collections;

public class BoulderEndLevelCheck : MonoBehaviour {

    public GameObject EndOfLevelText;

    private void OnTriggerEnter(Collider collider){
        if(collider.gameObject.name == "Boulder"){
            EndOfLevelText.SetActive(true);
        }
    }
}
