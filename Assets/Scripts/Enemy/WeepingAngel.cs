﻿using UnityEngine;
using System.Collections;

public class WeepingAngel : MonoBehaviour {

    public Transform target;
    Transform pos;
    float rayLength = 3;
    float speed = 2;
    bool move;

    void Start(){
        pos = transform;
        move = false;
    }

	void Update () {

        //If it cant see you, move is false
        if(renderer.isVisible){
           move = false;
        }

        //otherwise move is true
        if(!renderer.isVisible)
            move = true;

        //If it can see you, rotate the weeping angel only along the x axis
        if(move){
            var targetPoint = target.position;
            var targetRotation = Quaternion.LookRotation(targetPoint - transform.position, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 2.0f);
        }
	}
}
