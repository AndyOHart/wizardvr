﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterController))]
public class MovementController : MonoBehaviour {

    float movementSpeed = 4.0f;
    float rotationSpeed = 80.0f;
    float verticalVelocity = 0;
    float gravity = 5.0f;
    Vector3 fallSpeed = Vector3.zero;
    Component mainCam;

    CharacterController cc;

	// Use this for initialization
	void Start () {
        cc = GetComponent<CharacterController>();
        //mainCam = GetComponent("Main Camera");
        Screen.lockCursor = true;
	}
	
	// Update is called once per frame
	void Update () {

        float forwardSpeed = SixenseInput.Controllers[1].JoystickY * movementSpeed;
        Vector3 speed = new Vector3(verticalVelocity, 0, forwardSpeed);
        speed = transform.rotation * speed;

        if(SixenseInput.Controllers[1].JoystickY > 0){

            cc.Move(speed * Time.deltaTime);
        }

        if(SixenseInput.Controllers[1].JoystickY < 0){

            cc.Move(speed * Time.deltaTime);
        }

        if(SixenseInput.Controllers[1].JoystickX > 0){
            cc.transform.Rotate(0,(rotationSpeed * Time.deltaTime),0);
        }

        if(SixenseInput.Controllers[1].JoystickX < 0){

            cc.transform.Rotate(0,-(rotationSpeed * Time.deltaTime),0);
        }

        //Code for gravity to pull the player towards ground if they fall off something
        fallSpeed.y -= gravity * Time.deltaTime * 5;
        cc.Move(fallSpeed * Time.deltaTime);

	}

    public void SetSlowMotionSpeeds(float speed, float rotSpeed){

        movementSpeed = speed;
        rotationSpeed = rotSpeed;
    }

    void OnTriggerEnter(Collider collision){

        LightPowerup lightPowerupScript = collision.collider.gameObject.GetComponent<LightPowerup>();
        Light_Spell lightSpell = GameObject.Find("Wand").GetComponent<Light_Spell>();

        if(lightPowerupScript != null){
            lightSpell.AddTimeToTimer();
            Destroy(collision.gameObject);
        }
    }
}
