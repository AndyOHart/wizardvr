/*  HydraDeck camera code by Teddy0@gmail.com
  
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using UnityEngine;
using System.Collections;

public class HydraDeckHandController : SixenseHandController
{
    private Quaternion ModelRotation;

    private HydraDeck HydraDeckData;
    private GameObject HydraDeckPlayerController;

    new void Start()
    {
        base.Start();
        ModelRotation = transform.localRotation;
        HydraDeckPlayerController = GameObject.Find("HydraDeckPlayerController");
        FindHydraDeckData();
    }

    protected override void UpdateObject(SixenseInput.Controller controller)
    {
        if (!FindHydraDeckData())
            return;

        if (m_animator == null)
            return;

        if (controller.Enabled)
        {
            // Animation update
            UpdateAnimationInput(controller);
        }

        if (!m_enabled && HydraDeckData.State == HydraDeck.CameraState.Enabled)
        {
            // enable position and orientation control
            m_enabled = !m_enabled;
        }

        if (m_enabled)
        {
            gameObject.transform.position = HydraDeckPlayerController.transform.position - HydraDeckData.CurrentLeftOffset + HydraDeckData.GunPosition;
            gameObject.transform.rotation = HydraDeckData.GunRotation * ModelRotation;
        }
    }

    // Updates the animated object from controller input.
    new protected void UpdateAnimationInput(SixenseInput.Controller controller)
    {
        // Point
        if (controller.GetButton(SixenseButtons.TRIGGER))
        {
            m_animator.SetBool("Point", true);
        }
        else
        {
            m_animator.SetBool("Point", false);
        }

        // Idle
        if (m_animator.GetBool("Fist") == false &&
             m_animator.GetBool("HoldBook") == false &&
             m_animator.GetBool("GripBall") == false &&
             m_animator.GetBool("Point") == false)
        {
            m_animator.SetBool("Idle", true);
        }
        else
        {
            m_animator.SetBool("Idle", false);
        }
    }

    void OnGUI()
    {
    }

    //This is a good location for doing interactions, should be near the fingers
    public Vector3 GetInteractionPosition()
    {
        return transform.position + transform.forward * 0.1f;
    }

    public bool IsHandEnabled()
    {
        return m_enabled;
    }

    private bool FindHydraDeckData()
    {
        if (HydraDeckData == null)
        {
            GameObject HydraObject = GameObject.Find("HydraDeck");
            if (HydraObject == null)
                return false;
            HydraDeckData = HydraObject.GetComponent<HydraDeck>();
        }
        return true;
    }
}

