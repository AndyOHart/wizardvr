﻿/*  HydraDeck camera code by Teddy0@gmail.com
  
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(CharacterController))]

public class HydraDeckPlayerController : OVRComponent
{
	protected CharacterController 	Controller 		 = null;
    protected OVRCameraController   CameraController = null;
    protected HydraDeck             HydraDeckData    = null;

    public float Acceleration = 0.05f;
    public float Damping = 0.15f;
    public float JumpForce = 0.3f;
    public float GravityModifier = 0.379f;

    private float MoveScale = 1.0f;
    private Vector3 MoveThrottle = Vector3.zero;
    private float FallSpeed = 0.0f;
	
	// Awake
	new public virtual void Awake()
	{
		base.Awake();

        FindHydraDeckData();
		
		// We use Controller to move player around
		Controller = gameObject.GetComponent<CharacterController>();
        CameraController = gameObject.GetComponentInChildren<OVRCameraController>();
	}
		
	// Update 
	new public virtual void Update()
	{
		base.Update();

        if (!FindHydraDeckData())
            return;

        //Handle body position tracked movement
        var StartLocation = Controller.transform.position;
        var LeftOffset = HydraDeckData.BodyPosition;
        LeftOffset.y = 0;
        var OffsetApply = LeftOffset - HydraDeckData.CurrentLeftOffset;
        //Attempt to do the movement (with collision checking)
        Controller.Move(OffsetApply);
        //Track how far we actually moved
        HydraDeckData.CurrentLeftOffset = LeftOffset + (Controller.transform.position - (StartLocation + OffsetApply));
        HydraDeckData.CurrentLeftOffset.y = 0;
        //Add additional position offset to the camera controller, in case our body hit a wall but we don't want to stop the head moving
        CameraController.transform.position = Controller.transform.position + (HydraDeckData.BodyPosition - HydraDeckData.CurrentLeftOffset);

        //Handle Joystick movement
        float moveInfluence = Acceleration * 0.1f * DeltaTime;
        MoveThrottle.x += HydraDeckData.JoystickX * moveInfluence;
        MoveThrottle.z += HydraDeckData.JoystickY * moveInfluence;

        //Handle Jumping
        if (Controller.isGrounded && HydraDeckData.bJoyButtonPressed)
            MoveThrottle.y += JumpForce;

        //Apply damping
        float motorDamp = (1.0f + (Damping * DeltaTime));
        MoveThrottle.x /= motorDamp;
        MoveThrottle.y = (MoveThrottle.y > 0.0f) ? (MoveThrottle.y / motorDamp) : MoveThrottle.y;
        MoveThrottle.z /= motorDamp;

        //Move in the direction the body sensor is facing
        Vector3 moveDirection = HydraDeckData.BodyRotation * (MoveThrottle * DeltaTime);

        // Apply Gravity
        if (Controller.isGrounded && FallSpeed <= 0)
            FallSpeed = ((Physics.gravity.y * (GravityModifier * 0.002f)));
        else
            FallSpeed += ((Physics.gravity.y * (GravityModifier * 0.002f)) * DeltaTime);

        moveDirection.y += FallSpeed * DeltaTime;

        // Offset correction for uneven ground
        if( Controller.isGrounded && MoveThrottle.y <= 0.001f )
        {
            float bumpUpOffset = Mathf.Max(Controller.stepOffset,
                                     new Vector3(moveDirection.x, 0, moveDirection.z).magnitude);
            moveDirection -= bumpUpOffset * Vector3.up;
        }

        Vector3 predictedXZ = Vector3.Scale((Controller.transform.localPosition + moveDirection), new Vector3(1, 0, 1));

        // Move contoller
        Controller.Move(moveDirection);

        Vector3 actualXZ = Vector3.Scale(Controller.transform.localPosition, new Vector3(1, 0, 1));
        if (predictedXZ != actualXZ)
            MoveThrottle += (actualXZ - predictedXZ) / DeltaTime; 
	}

    private bool FindHydraDeckData()
    {
        if (HydraDeckData == null)
        {
            GameObject HydraObject = GameObject.Find("HydraDeck");
            if (HydraObject == null)
                return false;
            HydraDeckData = HydraObject.GetComponent<HydraDeck>();
        }
        return true;
    }
}

