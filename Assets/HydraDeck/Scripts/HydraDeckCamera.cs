﻿/*  HydraDeck camera code by Teddy0@gmail.com
  
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using UnityEngine;

public class HydraDeckCamera : MonoBehaviour
{
    public Vector3 HydraToWorldScale = new Vector3(0.001f, 0.001f, 0.001f);
    public float worldFloorY = 0.0f;

    public float JoystickRotationRate = 90.0f;

    public Vector3 m_gunModelOffset = new Vector3(0.0f, 0.075f, 0.0f);

    public RenderTexture HelpTextTexture = null;
    public GameObject HelpTextObject = null;

    private Quaternion StartCameraOffset;
    private Vector3 BodyRotationOffset = new Vector3(0,0,0);
    private OVRCameraController CameraController;
    private HydraDeck HydraDeckData;

    void Start ()
    {
        //Get the starting yaw of the camera when the scene starts
        StartCameraOffset = transform.rotation;
        //lol, why doesn't Unity have a Quaternion.Normalize function?
        StartCameraOffset.x = 0;
        StartCameraOffset.z = 0;
        StartCameraOffset = Quaternion.Lerp(StartCameraOffset, StartCameraOffset, 1);
        CameraController = GetComponent<OVRCameraController>();
        CameraController.SetOrientationOffset(StartCameraOffset);

        GameObject HydraObject = GameObject.Find("HydraDeck");
        if( HydraObject == null )
        {
            HydraObject = new GameObject("HydraDeck");
            HydraDeckData = HydraObject.AddComponent("HydraDeck") as HydraDeck;
            DontDestroyOnLoad(HydraDeckData);
        }
        else
        {
            HydraDeckData = HydraObject.GetComponent<HydraDeck>();
        }
        HydraDeckData.HelpTextTexture = HelpTextTexture;
        HydraDeckData.HelpTextObject = HelpTextObject;
        HydraDeckData.HydraFloorY = -860;
    }

    void OnGUI()
    {
        switch (HydraDeckData.State)
        {
            case HydraDeck.CameraState.Disabled:
                GUI.Box(new Rect(Screen.width/2 - 200, Screen.height/2, 400, 30), "Attach a Oculus Rift and Razer Hydra to play");
                break;

            case HydraDeck.CameraState.CalibrateFloor:
                GUI.Box(new Rect((7*Screen.width)/32, Screen.height/2, 200, 60), "Touch Right Hydra to Floor\nPull Right Trigger");
                GUI.Box(new Rect((20*Screen.width)/32, Screen.height/2, 200, 60), "Touch Right Hydra to Floor\nPull Right Trigger");
                break;

            case HydraDeck.CameraState.CalibrateChest:
                GUI.Box(new Rect((7 * Screen.width) / 32, Screen.height / 2, 200, 90), "Attach Left Hydra to Chest\nFace Forwards\nPull Right Trigger");
                GUI.Box(new Rect((20 * Screen.width) / 32, Screen.height / 2, 200, 90), "Attach Left Hydra to Chest\nFace Forwards\nPull Right Trigger");
                break;

            case HydraDeck.CameraState.CalibrateNeck:
                GUI.Box(new Rect((7 * Screen.width) / 32, Screen.height / 2, 200, 60), "Touch Hydra to back of neck\nPull Right Trigger");
                GUI.Box(new Rect((20 * Screen.width) / 32, Screen.height / 2, 200, 60), "Touch Hydra to back of neck\nPull Right Trigger");
                break;

            case HydraDeck.CameraState.Enabled:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        SixenseInput.Controller LeftController = SixenseInput.GetController(SixenseHands.LEFT);
        SixenseInput.Controller RightController = SixenseInput.GetController(SixenseHands.RIGHT);
        if( LeftController == null || RightController == null )
            return;

        switch (HydraDeckData.State)
        {
            case HydraDeck.CameraState.Disabled:
                if (SixenseInput.IsBaseConnected(0) && OVRDevice.SensorCount > 0)
                {
                    //If you have a normal mouse/keyboard camera controller, disable it here!
                    //FlightCamera myCamera = gameObject.GetComponent<MyCamera>();
                    //if (myCamera != null)
                    //    myCamera.enabled = false;

                    HydraDeckData.bEnabled = true;

                    HydraDeckData.State = HydraDeck.CameraState.CalibrateFloor;
                }
                break;

            case HydraDeck.CameraState.CalibrateFloor:
                if (RightController.GetButtonUp(SixenseButtons.TRIGGER))
                {
                    //Get the distance from the floor to the base
                    HydraDeckData.HydraFloorY = RightController.Position.y;

                    HydraDeckData.State = HydraDeck.CameraState.CalibrateChest;
                }
                break;

            case HydraDeck.CameraState.CalibrateChest:
                if (RightController.GetButtonUp(SixenseButtons.TRIGGER))
                {
                    //Get the Left Hydra's orientation offset
                    HydraDeckData.LeftRotationOffset = Quaternion.Inverse(LeftController.Rotation);
/*
                    //Get the base orientation of the head rotation, so it matches up
                    Quaternion BaseCameraYaw = new Quaternion(0,0,0,0);
                    OVRDevice.GetOrientation(0, ref BaseCameraYaw);
                    //lol, why doesn't Unity have a Quaternion.Normalize function?
                    BaseCameraYaw.x = 0;
                    BaseCameraYaw.z = 0;
                    BaseCameraYaw = Quaternion.Lerp(BaseCameraYaw, BaseCameraYaw, 1);

                    StartCameraOffset = StartCameraOffset * BaseCameraYaw;
*/
                    HydraDeckData.State = HydraDeck.CameraState.CalibrateNeck;
                }
                break;

            case HydraDeck.CameraState.CalibrateNeck:
                if (RightController.GetButtonUp(SixenseButtons.TRIGGER))
                {
                    HydraDeckData.ChestToNeckOffset = RightController.Position - LeftController.Position;

                    HydraDeckData.State = HydraDeck.CameraState.Enabled;
                }
                break;

            case HydraDeck.CameraState.Enabled:
                UpdateHydraButtons(LeftController, RightController);
                UpdateHydraCamera(LeftController, RightController);

                //Allow the Joystick to turn the world left/right when Right Bumper is held down.
                if (HydraDeckData.bBumper)
                {
                    StartCameraOffset *= Quaternion.AngleAxis(Time.deltaTime * RightController.JoystickX * JoystickRotationRate, Vector3.up);
                    HydraDeckData.JoystickX = 0;

                    //We need to recalculate the BodyPosition/Controller position, so it rotates around the body, not the Hydra Base
                    Vector3 OldBodyPosition = HydraDeckData.BodyPosition;
                    UpdateHydraCamera(LeftController, RightController);
                    BodyRotationOffset += OldBodyPosition - HydraDeckData.BodyPosition;
                }

                CameraController.SetOrientationOffset(StartCameraOffset);
                break;
        }
    }

    void UpdateHydraCamera(SixenseInput.Controller LeftController, SixenseInput.Controller RightController)
    {
        Quaternion FullBodyRotation = StartCameraOffset * LeftController.Rotation * HydraDeckData.LeftRotationOffset;

        //lol, why doesn't Unity have a Quaternion.Normalize function?
        Quaternion rotationToFaceForward = FullBodyRotation;
        rotationToFaceForward.x = 0;
        rotationToFaceForward.z = 0;
        rotationToFaceForward = Quaternion.Lerp(rotationToFaceForward, rotationToFaceForward, 1);

        //Get the position of the left controller, and subtract the floor height
        Vector3 HydraLocation = new Vector3(LeftController.Position.x,
                                            LeftController.Position.y - HydraDeckData.HydraFloorY,
                                            LeftController.Position.z);

        //Orientate it to our scene
        HydraLocation = StartCameraOffset * HydraLocation;

        //Add the rotated offset from left hyrda to neck
        Vector3 ChestToNeckLocal = FullBodyRotation * HydraDeckData.ChestToNeckOffset;
        HydraLocation += ChestToNeckLocal;

        //Apply the scaling from Hydra units to World units
        HydraLocation.Scale(HydraToWorldScale);

        HydraDeckData.BodyPosition = HydraLocation;
        HydraDeckData.BodyPosition.y += worldFloorY;

        HydraDeckData.BodyRotation = rotationToFaceForward;

        //Update the gun position/rotation
        Vector3 controllerPosition = new Vector3(RightController.Position.x,
                                                  RightController.Position.y - HydraDeckData.HydraFloorY,
                                                  RightController.Position.z);

        //Orientate it to our scene
        controllerPosition = StartCameraOffset * controllerPosition;
        controllerPosition.Scale(HydraToWorldScale);

        // update the localposition of the object
        HydraDeckData.GunRotation = StartCameraOffset * RightController.Rotation;
        HydraDeckData.GunPosition = controllerPosition + HydraDeckData.GunRotation * m_gunModelOffset;
        HydraDeckData.GunPosition.y += worldFloorY;

        // apply the rotation offset fixup
        HydraDeckData.BodyPosition += BodyRotationOffset;
        HydraDeckData.GunPosition += BodyRotationOffset;
    }

    void UpdateHydraButtons(SixenseInput.Controller LeftController, SixenseInput.Controller RightController)
    {
        //Output the button states
        HydraDeckData.bTriggerPressed = RightController.GetButtonDown(SixenseButtons.TRIGGER);
        HydraDeckData.bTriggerReleased = RightController.GetButtonUp(SixenseButtons.TRIGGER);
        HydraDeckData.bTrigger = RightController.GetButton(SixenseButtons.TRIGGER);

        HydraDeckData.bBumperPressed = RightController.GetButtonDown(SixenseButtons.BUMPER);
        HydraDeckData.bBumperReleased = RightController.GetButtonUp(SixenseButtons.BUMPER);
        HydraDeckData.bBumper = RightController.GetButton(SixenseButtons.BUMPER);

        HydraDeckData.bButton1Pressed = RightController.GetButtonDown(SixenseButtons.ONE);
        HydraDeckData.bButton1Released = RightController.GetButtonUp(SixenseButtons.ONE);
        HydraDeckData.bButton1 = RightController.GetButton(SixenseButtons.ONE);

        HydraDeckData.bButton2Pressed = RightController.GetButtonDown(SixenseButtons.TWO);
        HydraDeckData.bButton2Released = RightController.GetButtonUp(SixenseButtons.TWO);
        HydraDeckData.bButton2 = RightController.GetButton(SixenseButtons.TWO);

        HydraDeckData.bButton3Pressed = RightController.GetButtonDown(SixenseButtons.THREE);
        HydraDeckData.bButton3Released = RightController.GetButtonUp(SixenseButtons.THREE);
        HydraDeckData.bButton3 = RightController.GetButton(SixenseButtons.THREE);

        HydraDeckData.bButton4Pressed = RightController.GetButtonDown(SixenseButtons.FOUR);
        HydraDeckData.bButton4Released = RightController.GetButtonUp(SixenseButtons.FOUR);
        HydraDeckData.bButton4 = RightController.GetButton(SixenseButtons.FOUR);

        HydraDeckData.bStartPressed = RightController.GetButtonDown(SixenseButtons.START);
        HydraDeckData.bStartReleased = RightController.GetButtonUp(SixenseButtons.START);
        HydraDeckData.bStart = RightController.GetButton(SixenseButtons.START);

        HydraDeckData.bJoyButtonPressed = RightController.GetButtonDown(SixenseButtons.JOYSTICK);
        HydraDeckData.bJoyButtonReleased = RightController.GetButtonUp(SixenseButtons.JOYSTICK);
        HydraDeckData.bJoyButton = RightController.GetButton(SixenseButtons.JOYSTICK);

        HydraDeckData.JoystickX = HydraDeckData.bBumper ? 0.0f : RightController.JoystickX;
        HydraDeckData.JoystickY = RightController.JoystickY;
    }
}
